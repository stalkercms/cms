<?php

return [
    'driver' => env('MAIL_DRIVER', 'smtp'),
    'host' => env('MAIL_HOST', 'smtp.mandrillapp.com'),
    'port' => env('MAIL_PORT', 587),
    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'support@grapheme.ru'),
        'name' => env('MAIL_FROM_NAME', 'Grapheme CMS'),
    ],
    'encryption' => env('MAIL_ENCRYPTION', 'tls'),
    'username' => env('MAIL_USERNAME', 'info@grapheme.ru'),
    'password' => env('MAIL_PASSWORD', 'VRPb__VOJH44fZjWi4aZPA'),
    'sendmail' => '/usr/sbin/sendmail -bs',
    'pretend' => false,
];
