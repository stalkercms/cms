<?php

return [
    'driver' => env('SESSION_DRIVER', 'cookie'),
    'lifetime' => 3600,
    'expire_on_close' => false,
    'encrypt' => false,
    'files' => storage_path('framework/sessions'),
    'connection' => null,
    'table' => 'sessions',
    'lottery' => [2, 100],
    'cookie' => 'stalker_cms',
    'path' => '/',
    'domain' => null,
    'secure' => false,
];
