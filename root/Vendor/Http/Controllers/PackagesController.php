<?php
namespace STALKER_CMS\Vendor\Http\Controllers;

use STALKER_CMS\Vendor\Models\Packages;

/**
 * Контроллера по управлению пакетами CMS
 * Class PackagesController
 * @package STALKER_CMS\Vendor\Http\Controllers
 */
class PackagesController extends Controller {

    /**
     * Устанавливает указанный пакет
     * Установка или включение зависимых модулей
     * Запуск миграций модуля
     * @param Packages $package
     * @param bool $migration
     * @param bool $seeds
     * @return bool
     * @throws \Exception
     */
    public function installPackage(Packages $package, $migration = TRUE, $seeds = TRUE) {

        try {
            if(!empty($package->slug)):
                if(!empty($package->relations)):
                    foreach(explode('|', $package->relations) as $relationPackage_slug):
                        if($relationPackage = Packages::where('slug', $relationPackage_slug)->whereEnabled(FALSE)->first()):
                            if(empty($relationPackage->install_path)):
                                $this->installPackage($relationPackage);
                            else:
                                $this->enabledPackage($relationPackage);
                            endif;
                        endif;
                    endforeach;
                endif;
                $module_sections = self::makeModuleSections($package);
                $module_path = implode('/', $module_sections);
                $module_space = 'STALKER_CMS\\'.implode('\\', $module_sections);
                if($migration && file_exists(base_path('root/'.$module_path.'/Migrations'))):
                    \Artisan::call('migrate', ['--force' => TRUE, '--no-ansi' => TRUE, '--quiet' => TRUE, '--no-interaction' => TRUE, '--path' => 'root/'.$module_path.'/Migrations']);
                endif;
                if($seeds && file_exists(base_path('root/'.$module_path.'/Seeds/ModuleTableSeeder.php'))):
                    \Artisan::call('db:seed', ['--force' => TRUE, '--no-ansi' => TRUE, '--quiet' => TRUE, '--no-interaction' => TRUE, '--class' => $module_space.'\Seeds\ModuleTableSeeder']);
                endif;
                $package->install_path = $module_path;
                $package->enabled = TRUE;
                $package->save();
            else:
                return FALSE;
            endif;
        } catch(\Exception $e) {
            throw new \Exception(trans('root_lang::codes.500'), 500);
        }
    }

    /**
     * Включает указанный пакет
     * Включение зависимых модулей
     * @param Packages $package
     * @param bool $install
     * @return Packages
     */
    public function enabledPackage(Packages $package, $install = FALSE) {

        if(!empty($package->install_path)):
            foreach(explode('|', $package->relations) as $relationPackage_slug):
                if($relation_package = Packages::whereEnabled(FALSE)->whereRequired(FALSE)->whereSlug($relationPackage_slug)->first()):
                    $this->enabledPackage($relation_package);
                endif;
            endforeach;
            $package->enabled = TRUE;
            $package->touch();
            $package->save();
        elseif($install):
            $this->installPackage($package);
        endif;
        return $package;
    }

    /**
     * Выключает указанный пакет
     * Выключение зависимых модулей
     * @param Packages $package
     * @return Packages
     */
    public function disabledPackage(Packages $package) {

        if(!empty($package->install_path)):
            $package->enabled = FALSE;
            $package->touch();
            $package->save();
            if($relations_packages = Packages::whereEnabled(TRUE)->whereRequired(FALSE)->whereNotNull('relations')->get()):
                foreach($relations_packages as $relations_package):
                    foreach(explode('|', $relations_package->relations) as $relationPackage_slug):
                        if($relationPackage_slug == $package->slug):
                            $this->disabledPackage($relations_package);
                        endif;
                    endforeach;
                endforeach;
            endif;
        endif;
        return $package;
    }

    /**
     * Публикует ресурсы устанавливаемого пакета
     * @param Packages $package
     * @return bool
     */
    public function publishesPackageAssets(Packages $package) {

        $module_sections = $this->makeModuleSections($package);
        \Artisan::call('vendor:publish', ['--quiet' => TRUE, '--no-interaction' => TRUE, '--provider' => 'STALKER_CMS\\'.implode('\\', $module_sections).'\Providers\ModuleServiceProvider']);
        return TRUE;
    }

    /**
     * Возвращает системное имя пакета
     * @param $package
     * @return array
     */
    public static function makeModuleSections($package) {

        $module_sections = [];
        foreach(explode('_', $package['slug']) as $section):
            $module_sections[] = ucfirst($section);
        endforeach;
        /**
         * Если название пакета состоит из нескольких слов к примеру core_open_graph
         * то нужно выделить только два индекса
         * первый указывает на группу пакетов (core, application)
         * второй на название пакета (open_graph)
         */
        if(count($module_sections) > 2):
            $new_module_sections = $module_sections;
            unset($new_module_sections[0]);
            $module_sections = [
                $module_sections[0],
                implode('', $new_module_sections),
            ];
            return $module_sections;
        endif;
        return $module_sections;
    }
}