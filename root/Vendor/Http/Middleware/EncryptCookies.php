<?php
namespace STALKER_CMS\Vendor\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as BaseEncrypter;

/**
 * Фильтр сессии
 * Class EncryptCookies
 * @package STALKER_CMS\Vendor\Http\Middleware
 */
class EncryptCookies extends BaseEncrypter {

    protected $except = [];
}