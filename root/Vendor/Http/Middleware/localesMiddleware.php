<?php
namespace STALKER_CMS\Vendor\Http\Middleware;

use Closure;

/**
 * Фильтр локали
 * Устанавливает локаль для текущего пользователя
 * Class localesMiddleware
 * @package STALKER_CMS\Vendor\Http\Middleware
 */
class localesMiddleware {

    public function handle($request, Closure $next, $package = NULL, $module = NULL, $setting = NULL) {

        if(\Auth::guard()->check()):
            \App::setLocale(\Auth::user()->locale);
        endif;
        return $next($request);
    }
}