<?php
namespace STALKER_CMS\Vendor\Interfaces;

/**
 * Интерфейс RESTful контроллеров
 * Interface CrudInterface
 * @package STALKER_CMS\Vendor\Interfaces
 */
interface CrudInterface {

    public function index();

    public function create();

    public function store();

    public function edit($id);

    public function update($id);

    public function destroy($id);
}