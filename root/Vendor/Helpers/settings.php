<?php
/**
 * Возвращает значение параметра настройки модуля
 * Используется сингилтон "Settings"
 * @param $attributes - массив или название настройки модуля. Может содержать до 3х элементов: название пакета, название модуля в пакете, название настройки
 * @param string $return_field - возвращаемое значение настройки
 * @return null
 */
if(!function_exists('settings')) {
    function settings($attributes, $return_field = 'value') {

        try {
            $_settings = \App::make('Settings');
            $setting_value = NULL;
            if(is_array($attributes) == FALSE):
                $attributes = (array)$attributes;
            endif;
            if(is_array($attributes)):
                switch(count($attributes)):
                    case 3 :
                        $setting_value = isset($_settings[@$attributes[0]][@$attributes[1]]['options'][@$attributes[2]][$return_field])
                            ? $_settings[@$attributes[0]][@$attributes[1]]['options'][@$attributes[2]][$return_field]
                            : NULL;
                        break;
                    case 2 :
                        foreach($_settings as $package_name => $modules):
                            foreach($modules as $module_name => $settings):
                                if($module_name == @$attributes[0]):
                                    $setting_value = isset($settings['options'][@$attributes[1]][$return_field])
                                        ? $settings['options'][@$attributes[1]][$return_field]
                                        : NULL;
                                endif;
                            endforeach;
                        endforeach;
                        break;
                    case 1 :
                        foreach($_settings as $package_name => $modules):
                            foreach($modules as $module_name => $settings):
                                foreach($settings['options'] as $field_name => $fields):
                                    if($field_name == @$attributes[0] && isset($fields[$return_field])):
                                        $setting_value = $fields[$return_field];
                                    endif;
                                endforeach;
                            endforeach;
                        endforeach;
                        break;
                endswitch;
            endif;
            return $setting_value;
        } catch(\Exception $e) {
            return NULL;
        }
    }
}
/**
 * Обновляет файл конфигурации
 * @param $config_params
 * @param $config_value
 * @return bool
 */
if(!function_exists('update_config_file')) {
    function update_config_file($config_params, $config_value) {

        $config_file = file(base_path('.env'));
        if(is_array($config_file)):
            foreach($config_file as $key => $value):
                $config_file[$key] = preg_replace("/$config_params/", $config_value, $value);
            endforeach;
        endif;
        $fp = fopen(base_path('.env'), "w+");
        fwrite($fp, implode("", $config_file));
        fclose($fp);
        return TRUE;
    }
}
/**
 * Включает отображение ошибок
 */
if(!function_exists('debug')) {
    function debug() {

        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        ini_set('memory_limit', '-1');
    }
}