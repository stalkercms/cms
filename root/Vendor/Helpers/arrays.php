<?php
/**
 * Возвращает текст по ключу массива исходя из текущей локали
 * @param array $trans
 * @return mixed
 */
if(!function_exists('array_translate')) {
    function array_translate(array $trans) {

        if(isset($trans[\App::getLocale()])):
            return $trans[\App::getLocale()];
        elseif(isset($trans[config('app.fallback_locale')])):
            return $trans[config('app.fallback_locale')];
        else:
            return 'Undefined';
        endif;
    }
}
/**
 * Рекурсивный обход массива
 * Возвращает массив в виде "дерева"
 * @param $rs
 * @param $parent
 * @return array
 */
if(!function_exists('recursive')) {
    function recursive(&$rs, $parent) {

        $out = [];
        if(!isset($rs[$parent])):
            return $out;
        endif;
        foreach($rs[$parent] as $row):
            $chidls = recursive($rs, $row['id']);
            if($chidls):
                $row['sub_menu'] = $chidls;
            endif;
            $out[] = $row;
        endforeach;
        return $out;
    }
}
/**
 * Сортировка многомерного массива
 * http://php.net/manual/ru/function.array-multisort.php#91638
 * @param $array
 * @param $cols
 * @return array
 */
if(!function_exists('array_msort')) {
    function array_msort($array, $cols) {

        $colarr = [];
        foreach($cols as $col => $order) {
            $colarr[$col] = [];
            foreach($array as $k => $row) {
                $colarr[$col]['_'.$k] = strtolower($row[$col]);
            }
        }
        $eval = 'array_multisort(';
        foreach($cols as $col => $order) {
            $eval .= '$colarr[\''.$col.'\'],'.$order.',';
        }
        $eval = substr($eval, 0, -1).');';
        eval($eval);
        $ret = [];
        foreach($colarr as $col => $arr) {
            foreach($arr as $k => $v) {
                $k = substr($k, 1);
                if(!isset($ret[$k])) {
                    $ret[$k] = $array[$k];
                }
                $ret[$k][$col] = $array[$k][$col];
            }
        }
        return $ret;
    }
}
/**
 * Возвращает массив состоящий из строк CSV-файла
 * @param string $filename
 * @param string $delimiter
 * @return array|bool
 */
if(!function_exists('csv_to_array')) {
    function csv_to_array($filename = '', $delimiter = ';') {

        if(!file_exists($filename) || !is_readable($filename)) {
            return FALSE;
        }
        $header = NULL;
        $data = [];
        if(($handle = fopen($filename, 'r')) !== FALSE) {
            while(($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                if(!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }
        return $data;
    }
}
/**
 * Создает CSV-файл состоящий из элементов массива
 * @param array $array
 * @param string $filename
 * @param string $delimiter
 * @param string $in_charset
 * @param string $out_charset
 * @return bool
 */
if(!function_exists('array_to_csv')) {
    function array_to_csv(array $array, $filename = 'export.csv', $delimiter = ';', $in_charset = 'UTF-8', $out_charset = 'Windows-1251') {

        $scv_file = fopen($filename, 'w');
        foreach($array as $fields):
            foreach($fields as $field => $value):
                $fields[$field] = iconv($in_charset, $out_charset, (string)$value);
            endforeach;
            fputcsv($scv_file, $fields, $delimiter);
        endforeach;
        fclose($scv_file);
        if(file_exists($filename)):
            return TRUE;
        else:
            return FALSE;
        endif;
    }
}