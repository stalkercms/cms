<?php
/**
 * Выводит информацию о правах на файл
 * @param $fn
 * @return string
 */
if(!function_exists('file_info')) {
    function file_info($fn) {

        $perms = fileperms($fn);
        // Owner
        $info = (($perms & 0x0100) ? 'r' : '-');
        $info .= (($perms & 0x0080) ? 'w' : '-');
        $info .= (($perms & 0x0040) ? (($perms & 0x0800) ? 's' : 'x') : (($perms & 0x0800) ? 'S' : '-'));
        // Group
        $info .= (($perms & 0x0020) ? 'r' : '-');
        $info .= (($perms & 0x0010) ? 'w' : '-');
        $info .= (($perms & 0x0008) ? (($perms & 0x0400) ? 's' : 'x') : (($perms & 0x0400) ? 'S' : '-'));
        // World
        $info .= (($perms & 0x0004) ? 'r' : '-');
        $info .= (($perms & 0x0002) ? 'w' : '-');
        $info .= (($perms & 0x0001) ? (($perms & 0x0200) ? 't' : 'x') : (($perms & 0x0200) ? 'T' : '-'));
        return $info;
    }
}
/**
 * Возвращает содержимое каталога в массив
 * @param $directory
 * @param $recursive
 * @return array
 */
if(!function_exists('directoryToArray')) {
    function directoryToArray($directory, $recursive) {

        $array_items = [];
        if($handle = opendir($directory)):
            while(FALSE !== ($file = readdir($handle))):
                if($file != "." && $file != ".."):
                    if(is_dir($directory."/".$file)):
                        if($recursive):
                            $array_items = array_merge($array_items, directoryToArray($directory."/".$file, $recursive));
                        endif;
                        $file = $directory."/".$file;
                        $perms = file_info($file);
                        $array_items[] = preg_replace("/\/\//si", "/", $file."\t".$perms);
                    else:
                        $file = $directory."/".$file;
                        $perms = file_info($file);
                        $array_items[] = preg_replace("/\/\//si", "/", $file."\t".$perms);
                    endif;
                endif;
            endwhile;
            closedir($handle);
        endif;
        return $array_items;
    }
}
/**
 * Создает новый каталог в каталоге загрузок
 * @param $directory
 * @return mixed
 * @throws \Exception
 */
if(!function_exists('setDirectory')) {
    function setDirectory($directory) {

        if(empty($directory)):
            throw new \Exception(trans('root_lang::codes.2700'), 500);
        endif;
        if(\Storage::exists($directory) === FALSE):
            \Storage::makeDirectory($directory, 0754, TRUE);
        endif;
        return $directory;
    }
}
/**
 * Возвращает размер каталога
 * @param $path
 * @return bool|int
 */
if(!function_exists('getDirSize')) {
    function getDirSize($path) {

        $returnSize = 0;
        if(!$h = @opendir($path)):
            return FALSE;
        endif;
        while(($element = readdir($h)) !== FALSE):
            if($element <> "." and $element <> ".."):
                $all_path = $path."/".$element;
                if(@filetype($all_path) == "file"):
                    $returnSize += filesize($all_path);
                elseif(@filetype($all_path) == "dir"):
                    @$returnSize += getDirSize($all_path);
                endif;
            endif;
        endwhile;
        return $returnSize;
    }
}