<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
    {!! Html::meta(['charset' => 'utf-8', 'content' => NULL, 'name' => NULL]) !!}
    {!! Html::meta(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1']) !!}
    <title>@yield('title', config('app.application_name'))</title>
    <meta name="description" content="@yield('description')">
    {!! Html::meta(['name' => 'csrf-token', 'content' => csrf_token()]) !!}
    {!! Html::meta(['name' => 'locale', 'content' => \App::getLocale()]) !!}
    {!! Html::meta(['name' => 'fallback_locale', 'content' => config('app.fallback_locale')]) !!}
    {!! Html::coreVendorCSS() !!}
    {!! Html::coreMainCSS() !!}
    {!! Html::favicon() !!}
</head>
<body>
@yield('content')
@include('root_views::assets.old-browser')
{!! Html::coreVendorJS() !!}
@yield('scripts_before')
{!! Html::coreMainJS() !!}
@yield('scripts_after')
</body>
</html>