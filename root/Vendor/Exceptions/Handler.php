<?php
namespace STALKER_CMS\Vendor\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

/**
 * Class Handler
 * Отслеживание исключений и отображения страниц-ошибок или отладочной информации
 * https://laravel.com/docs/5.2/errors
 * @package STALKER_CMS\Vendor\Exceptions
 */
class Handler extends ExceptionHandler {

    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class
    ];

    /**
     * Метод используется для логирования исключений
     * @param Exception $e
     */
    public function report(Exception $e) {

        return parent::report($e);
    }

    /**
     * Обрабатывает исключения
     * @param \Illuminate\Http\Request $request
     * @param Exception $exception
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|SymfonyResponse
     */
    public function render($request, \Exception $exception) {

        $statusCode = $this->getStatusCode($exception);
        if($request->wantsJson()):
            if(config('app.debug') === FALSE):
                $errorText = trans('root_lang::codes.'.$statusCode);
                if(empty($errorText)):
                    $errorText = $exception->getMessage();
                endif;
                return \ResponseController::error($statusCode)->set('errorText', $errorText)->json();
            else:
                return parent::render($request, $exception);
            endif;
        endif;
        $this->BugNotify($exception);
        return $this->convertExceptionToResponse($exception);
    }

    /**
     * @param Exception $e
     * Возвращает страницу с ошибкой или страницу с отладочной информацией
     * в зависимости от включенного режима отладки
     * @return \Illuminate\Http\Response|SymfonyResponse
     */
    protected function convertExceptionToResponse(Exception $e) {

        if(config('app.debug')):
            return parent::convertExceptionToResponse($e);
        else:
            $statusCode = $this->getStatusCode($e);
            if(view()->exists("home_views::errors.$statusCode")):
                return response()->view(
                    "home_views::errors.$statusCode",
                    ['code' => $statusCode, 'message' => trans('root_lang::codes.'.$statusCode)],
                    $statusCode <= 1000 ? $statusCode : 500);
            endif;
            if(view()->exists("root_views::errors.$statusCode")):
                return response()->view(
                    "root_views::errors.$statusCode",
                    ['code' => $statusCode, 'message' => trans('root_lang::codes.'.$statusCode)],
                    $statusCode <= 1000 ? $statusCode : 500);
            else:
                return response()->view(
                    "root_views::errors.2000", ['code' => 2000, 'message' => $e->getMessage()], 500);
            endif;
        endif;
    }

    /**
     * @param Exception $e
     * Возвращает код ошибкив зависимости от типа исключения
     * @return int|mixed|string
     */
    protected function getStatusCode(\Exception $e) {

        if(file_exists(base_path('.env')) === FALSE):
            return 2005;
        elseif($e instanceof ModelNotFoundException):
            return 404;
        elseif($e instanceof NotFoundHttpException):
            return 404;
        elseif($e instanceof \InvalidArgumentException):
            return 404;
        elseif($e instanceof MethodNotAllowedHttpException):
            return 405;
        elseif($e instanceof HttpException):
            return $e->getStatusCode();
        elseif($e->getCode() > 0):
            return $e->getCode();
        else:
            return 500;
        endif;
    }

    /**
     * Регистрация ошибки и отправка уведомления
     * @param Exception $e
     * @return bool
     */
    private function BugNotify(\Exception $e) {

        if(class_exists('PermissionsController')):
            if(\PermissionsController::isPackageEnabled('solutions_bugnotify')):
                $code = $this->getStatusCode($e);
                \Bugnotify::setStatusCode($code)->Notifier($e)->logging($e);
            endif;
        endif;
    }
}
