<?php

namespace STALKER_CMS\Vendor\Traits;

/**
 * Работа с curl запросами
 * Class CurlTrait
 * @package STALKER_CMS\Vendor\Traits
 */
trait CurlTrait {

    /**
     * GET запрос
     * @param $url
     * @param array $headers
     * @return mixed
     */
    public function getCurl($url, $headers = []) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        if (!empty($headers)):
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        endif;
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $data['curl_result'] = curl_exec($ch);
        $data['curl_info'] = curl_getinfo($ch);
        if (!empty($headers)):
            $data['curl_headers'] = $headers;
        endif;
        curl_close($ch);
        return $data;
    }

    /**
     * POST запрос
     * @param $url
     * @param null $post_data
     * @param array $headers
     * @return mixed
     */
    public function postCurl($url, $post_data = NULL, $headers = []) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        if (!empty($headers)):
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        endif;
        if (!empty($post_data)):
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        endif;
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $data['curl_result'] = curl_exec($ch);
        $data['curl_info'] = curl_getinfo($ch);
        if (!is_null($post_data)):
            $data['curl_data'] = $post_data;
        endif;
        if (!empty($headers)):
            $data['curl_headers'] = $headers;
        endif;
        curl_close($ch);
        return $data;
    }

    /**
     * PUT запрос
     * @param $url
     * @param array $post_data
     * @param array $headers
     * @return mixed
     */
    public function putCurl($url, $post_data = [], $headers = []) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        if (!empty($headers)):
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        endif;
        if (!empty($post_data)):
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        endif;
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $data['curl_result'] = curl_exec($ch);
        $data['curl_info'] = curl_getinfo($ch);
        if (!empty($headers)):
            $data['curl_headers'] = $headers;
        endif;
        curl_close($ch);
        return $data;
    }
}