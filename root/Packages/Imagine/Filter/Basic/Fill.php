<?php
namespace STALKER_CMS\Packages\Imagine\Filter\Basic;

use STALKER_CMS\Packages\Imagine\Filter\FilterInterface;
use STALKER_CMS\Packages\Imagine\Image\Fill\FillInterface;
use STALKER_CMS\Packages\Imagine\Image\ImageInterface;

/**
 * A fill filter
 */
class Fill implements FilterInterface {

    /**
     * @var FillInterface
     */
    private $fill;

    /**
     * @param FillInterface $fill
     */
    public function __construct(FillInterface $fill) {

        $this->fill = $fill;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(ImageInterface $image) {

        return $image->fill($this->fill);
    }
}
