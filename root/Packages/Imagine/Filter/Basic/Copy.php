<?php
namespace STALKER_CMS\Packages\Imagine\Filter\Basic;

use STALKER_CMS\Packages\Imagine\Filter\FilterInterface;
use STALKER_CMS\Packages\Imagine\Image\ImageInterface;

/**
 * A copy filter
 */
class Copy implements FilterInterface {

    /**
     * {@inheritdoc}
     */
    public function apply(ImageInterface $image) {

        return $image->copy();
    }
}
