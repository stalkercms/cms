<?php
namespace STALKER_CMS\Packages\Imagine\Image\Metadata;

use STALKER_CMS\Packages\Imagine\Exception\InvalidArgumentException;
use STALKER_CMS\Packages\Imagine\Exception\NotSupportedException;

/**
 * Metadata driven by Exif information
 */
class ExifMetadataReader extends AbstractMetadataReader {

    public function __construct() {

        if(!self::isSupported()) {
            throw new NotSupportedException('PHP exif extension is required to use the ExifMetadataReader');
        }
    }

    public static function isSupported() {

        return function_exists('exif_read_data');
    }

    /**
     * {@inheritdoc}
     */
    protected function extractFromFile($file) {

        if(FALSE === $data = @file_get_contents($file)) {
            throw new InvalidArgumentException(sprintf('File %s is not readable.', $file));
        }
        return $this->doReadData($data);
    }

    /**
     * {@inheritdoc}
     */
    protected function extractFromData($data) {

        return $this->doReadData($data);
    }

    /**
     * {@inheritdoc}
     */
    protected function extractFromStream($resource) {

        if(0 < ftell($resource)) {
            $metadata = stream_get_meta_data($resource);
            if($metadata['seekable']) {
                rewind($resource);
            }
        }
        return $this->doReadData(stream_get_contents($resource));
    }

    /**
     * Extracts metadata from raw data, merges with existing metadata
     *
     * @param string $data
     *
     * @return MetadataBag
     */
    private function doReadData($data) {

        if(substr($data, 0, 2) === 'II') {
            $mime = 'image/tiff';
        } else {
            $mime = 'image/jpeg';
        }
        return $this->extract('data://'.$mime.';base64,'.base64_encode($data));
    }

    /**
     * Performs the exif data extraction given a path or data-URI representation.
     *
     * @param string $path The path to the file or the data-URI representation.
     *
     * @return MetadataBag
     */
    private function extract($path) {

        if(FALSE === $exifData = @exif_read_data($path, NULL, TRUE, FALSE)) {
            return array();
        }
        $metadata = array();
        $sources = array('EXIF' => 'exif', 'IFD0' => 'ifd0');
        foreach($sources as $name => $prefix) {
            if(!isset($exifData[$name])) {
                continue;
            }
            foreach($exifData[$name] as $prop => $value) {
                $metadata[$prefix.'.'.$prop] = $value;
            }
        }
        return $metadata;
    }
}
