<?php
namespace STALKER_CMS\Packages\Imagine\Image\Metadata;

use STALKER_CMS\Packages\Imagine\Exception\InvalidArgumentException;

interface MetadataReaderInterface {

    /**
     * Reads metadata from a file.
     *
     * @param $file The path to the file where to read metadata.
     *
     * @throws InvalidArgumentException In case the file does not exist.
     *
     * @return MetadataBag
     */
    public function readFile($file);

    /**
     * Reads metadata from a binary string.
     *
     * @param $data The binary string to read.
     *
     * @return MetadataBag
     */
    public function readData($data);

    /**
     * Reads metadata from a stream.
     *
     * @param $resource The stream to read.
     *
     * @throws InvalidArgumentException In case the resource is not valid.
     *
     * @return MetadataBag
     */
    public function readStream($resource);
}
