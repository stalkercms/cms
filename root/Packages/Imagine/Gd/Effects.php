<?php
namespace STALKER_CMS\Packages\Imagine\Gd;

use STALKER_CMS\Packages\Imagine\Effects\EffectsInterface;
use STALKER_CMS\Packages\Imagine\Exception\RuntimeException;
use STALKER_CMS\Packages\Imagine\Image\Palette\Color\ColorInterface;
use STALKER_CMS\Packages\Imagine\Image\Palette\Color\RGB as RGBColor;

/**
 * Effects implementation using the GD library
 */
class Effects implements EffectsInterface {

    private $resource;

    public function __construct($resource) {

        $this->resource = $resource;
    }

    /**
     * {@inheritdoc}
     */
    public function gamma($correction) {

        if(FALSE === imagegammacorrect($this->resource, 1.0, $correction)) {
            throw new RuntimeException('Failed to apply gamma correction to the image');
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function negative() {

        if(FALSE === imagefilter($this->resource, IMG_FILTER_NEGATE)) {
            throw new RuntimeException('Failed to negate the image');
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function grayscale() {

        if(FALSE === imagefilter($this->resource, IMG_FILTER_GRAYSCALE)) {
            throw new RuntimeException('Failed to grayscale the image');
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function colorize(ColorInterface $color) {

        if(!$color instanceof RGBColor) {
            throw new RuntimeException('Colorize effects only accepts RGB color in GD context');
        }
        if(FALSE === imagefilter($this->resource, IMG_FILTER_COLORIZE, $color->getRed(), $color->getGreen(), $color->getBlue())) {
            throw new RuntimeException('Failed to colorize the image');
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function sharpen() {

        $sharpenMatrix = array(array(-1, -1, -1), array(-1, 16, -1), array(-1, -1, -1));
        $divisor = array_sum(array_map('array_sum', $sharpenMatrix));
        if(FALSE === imageconvolution($this->resource, $sharpenMatrix, $divisor, 0)) {
            throw new RuntimeException('Failed to sharpen the image');
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function blur($sigma = 1) {

        if(FALSE === imagefilter($this->resource, IMG_FILTER_GAUSSIAN_BLUR)) {
            throw new RuntimeException('Failed to blur the image');
        }
        return $this;
    }
}
