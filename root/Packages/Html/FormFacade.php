<?php
namespace STALKER_CMS\Packages\Html;

use Illuminate\Support\Facades\Facade;

class FormFacade extends Facade {

    protected static function getFacadeAccessor() {
        return 'form';
    }
}