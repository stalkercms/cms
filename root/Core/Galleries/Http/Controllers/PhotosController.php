<?php
namespace STALKER_CMS\Core\Galleries\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Core\Galleries\Models\Gallery;
use STALKER_CMS\Core\Galleries\Models\Photo;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Контроллер Изображение гелереи
 * Class PhotosController
 * @package STALKER_CMS\Core\Galleries\Http\Controllers
 */
class PhotosController extends ModuleController implements CrudInterface {

    use ModelTrait;
    protected $model;
    protected $gallery;

    /**
     * PhotosController constructor.
     * @param Photo $photo
     * @param Gallery $gallery
     */
    public function __construct(Photo $photo, Gallery $gallery) {

        $this->model = $photo;
        $this->gallery = $gallery;
        $this->middleware('auth');
    }

    /**
     * Просмотр галереи
     * @param null $id - ID галереи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id = NULL) {

        \PermissionsController::allowPermission('core_galleries', 'images');
        $gallery = $this->gallery->whereFind(['id' => $id]);
        $photos = $gallery->photos()->orderBy('order')->get();
        $allowed_types = [];
        foreach(self::getAllowedFileTypes() as $type):
            $allowed_types[] = $type['mime'];
        endforeach;
        return view('core_galleries_views::photos.index',
            [
                'gallery' => $gallery,
                'photos' => $photos,
                'allowed_types' => $allowed_types
            ]
        );
    }

    /**
     * Загрузка файла на сервер
     * @param null $id - ID галереи
     * @return mixed
     */
    public function store($id = NULL) {

        \PermissionsController::allowPermission('core_galleries', 'images');
        $request = \RequestController::isAJAX()->init();
        $valid = \GalleryValidatorController::init($request::file('file'));
        if($valid->passes()):
            $gallery = $this->gallery->whereFind(['id' => $id]);
            if($image = $this->uploadImage($request, $gallery)):
                $image['gallery_id'] = $id;
                $image['photo'] = NULL;
                $image['thumbnail_path'] = $this->makeThumbnail($request);
                $image['order'] = $this->model->nextOrder($id);
                $request::merge($image);
                $this->model->insert($request);
            endif;
            return \ResponseController::success(200)->redirect(route('core.galleries.photos_index', $id))->json();
        else:
            return \ResponseController::error(0)->set('errorText', $valid->getMessage())->json();
        endif;
    }

    /**
     *
     */
    public function create() {
        // TODO: Implement create() method.
    }

    /**
     * @param $id
     */
    public function edit($id) {
        // TODO: Implement edit() method.
    }

    /**
     * @param $id
     */
    public function update($id) {
        // TODO: Implement update() method.
    }

    public function updateLabels() {

        \PermissionsController::allowPermission('core_galleries', 'images');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['photo_id' => 'required'])):
            if($photo = $this->model->whereFind(['id' => $request::input('photo_id')])):
                $photo->title = $request::input('title');
                $photo->alt = $request::input('alt');
                $photo->save();
            endif;
            return \ResponseController::success(200)->redirect(route('core.galleries.photos_index', $photo->gallery->id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \PermissionsController::allowPermission('core_galleries', 'images');
        $request = \RequestController::isAJAX()->init();
        if(is_numeric($id)):
            $this->model->remove($id);
            return \ResponseController::success(1203)->redirect(route('core.galleries.photos_index', $request::input('gallery')))->json();
        elseif(is_string($id) && $id == 'selected'):
            $this->deleteFiles($this->model->whereIn('id', $request::input('files'))->get());
            return \ResponseController::success(1203)->redirect(route('core.galleries.photos_index', $request::input('gallery')))->json();
        elseif(is_string($id) && $id == 'all'):
            $this->deleteFiles($this->model->whereGalleryId($request::input('gallery'))->get());
            return \ResponseController::success(1203)->redirect(route('core.galleries.photos_index', $request::input('gallery')))->json();
        endif;
        return \ResponseController::error(2503)->json();
    }

    /**
     * Загрузка изображения или создание из base64-строки
     * @param $request
     * @param Gallery $gallery
     * @return mixed
     * @throws \Exception
     */
    public function uploadImage($request, Gallery $gallery) {

        if($request::hasFile('file')):
            $upload_directory = setDirectory(settings(['core_galleries', 'galleries', 'photo_dir']));
            $fileName = time()."_".rand(1000, 1999).'.'.$request::file('file')->getClientOriginalExtension();
            $photoPath = double_slash($upload_directory.'/'.$fileName);
            $uploaded['original_name'] = $request::file('file')->getClientOriginalName();
            $request::file('file')->move('uploads/'.$upload_directory, $fileName);
            $uploaded['path'] = add_first_slash($photoPath);
            $uploaded['thumbnail_path'] = NULL;
            $uploaded['file_size'] = \Storage::size($photoPath);
            $uploaded['mime_type'] = \Storage::mimeType($photoPath);
            return $uploaded;
        endif;
    }

    public function makeThumbnail(\Request $request) {

        if($thumbnail = trim_base64_image($request::input('thumbnail'))):
            $upload_directory_thumbnail = setDirectory(settings(['core_galleries', 'galleries', 'thumbnail_dir']));
            $fileName = time()."_".rand(1000, 1999).'.jpg';
            $thumbnailPath = double_slash($upload_directory_thumbnail.'/'.$fileName);
            \Storage::put($thumbnailPath, base64_decode($thumbnail));
            return add_first_slash($thumbnailPath);
        endif;
        return NULL;
    }

    /**
     * Сортировка изображений в галереи
     * @return \Illuminate\Http\JsonResponse
     */
    public function sortable() {

        \PermissionsController::allowPermission('core_galleries', 'images');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['elements' => 'required'])):
            foreach(explode(',', $request::input('elements')) as $index => $photo_id):
                Photo::where('id', $photo_id)->update(['order' => $index + 1]);
            endforeach;
            return \ResponseController::success(202)->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /************************************************************************************/
    /**
     * @param Collection $files
     */
    private function deleteFiles(Collection $files) {

        foreach($files as $file):
            $this->model->remove($file->id);
        endforeach;
    }
}