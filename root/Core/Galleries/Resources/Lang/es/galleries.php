<?php
return [
    'list' => 'Hoja de galerías',
    'pictures' => 'Fotos',
    'edit' => 'Editar',
    'embed' => 'Código de inserción',
    'update' => 'Fecha de actualización',
    'slug' => 'Simbólico código',
    'search' => 'Introduzca el nombre de la galería',
    'sort_title' => 'Título',
    'sort_published' => 'Fecha de creación',
    'sort_updated' => 'Fecha de actualización',
    'delete' => [
        'question' => 'Eliminar galería',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'missing' => 'Faltan imágenes',
    'pictures_count' => 'foto|fotos|fotos',
    'empty' => 'Lista está vacía',
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de la galería',
        'form' => [
            'title' => 'Título',
            'title_help_description' => 'Por ejemplo: Galería Principal',
            'template' => 'Galería de plantillas',
            'description' => 'Descripción',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: galería_principal',
            'wight_thumbnail' => 'Miniatura ancho fijo en píxeles',
            'height_thumbnail' => 'Altura fija de la miniatura en píxeles',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar galería',
        'form' => [
            'title' => 'Título',
            'title_help_description' => 'Por ejemplo: Galería Principal',
            'template' => 'Galería de plantillas',
            'description' => 'Descripción',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: galería_principal',
            'wight_thumbnail' => 'Miniatura ancho fijo en píxeles',
            'height_thumbnail' => 'Altura fija de la miniatura en píxeles',
            'submit' => 'Guardar'
        ]
    ]
];