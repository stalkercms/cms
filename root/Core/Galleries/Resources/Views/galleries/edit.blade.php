@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_galleries::menu.icon') }}"></i> {!! array_translate(config('core_galleries::menu.title')) !!}
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('core_galleries_lang::galleries.replace.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-edit"></i> @lang('core_galleries_lang::galleries.replace.title')</h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            {!! Form::model($gallery,['route' => ['core.galleries.update', $gallery->id], 'class' => 'form-validate', 'id' => 'edit-gallery-form', 'method' => 'PUT']) !!}
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('core_galleries_lang::galleries.replace.form.title')</label>
                        </div>
                        <small class="help-description">@lang('core_galleries_lang::galleries.replace.form.title_help_description')</small>
                    </div>
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('core_galleries_lang::galleries.replace.form.template')</p>
                        {!! Form::select('template_id', $templates, NULL,['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    {!! Form::text('wight_thumbnail', 200, ['class'=>'input-sm form-control fg-input input-numeric']) !!}
                                    <label class="fg-label">@lang('core_galleries_lang::galleries.replace.form.wight_thumbnail')</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    {!! Form::text('height_thumbnail', 200, ['class'=>'input-sm form-control fg-input input-numeric']) !!}
                                    <label class="fg-label">@lang('core_galleries_lang::galleries.replace.form.height_thumbnail')</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <p class="c-gray m-b-20">@lang('core_galleries_lang::galleries.replace.form.description')</p>
                                {!! Form::textarea('description', NULL, ['id' => 'description']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary btn-sm m-t-10 waves-effect waves-effect" autocomplete="off" type="submit">
                <i class="fa fa-save"></i>
                <span class="btn-text">@lang('core_galleries_lang::galleries.replace.form.submit')</span>
            </button>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('scripts_after')
    @set($summernote_locale, \App::getLocale() . '-' . strtoupper(\App::getLocale()))
    {!! Html::script('core/summernote/summernote-' . $summernote_locale . '.js') !!}
    <script>
        $("#description").summernote({
            height: 150,
            tabsize: 2,
            lang: '{{ $summernote_locale }}',
            toolbar: [
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture']],
                ['view', ['fullscreen', 'codeview']],
            ]
        });
        $('#description').on('summernote.change', function (we, contents, $editable) {
            $("#description").html(contents);
            $("#description").change();
        });
    </script>
@stop