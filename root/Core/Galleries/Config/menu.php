<?php
return [
    'package' => 'core_galleries',
    'title' => ['ru' => 'Галереи', 'en' => 'Galleries', 'es' => 'Galerías'],
    'route' => '__#',
    'icon' => 'zmdi zmdi-collection-image',
    'menu_child' => [
        'galleries' => [
            'title' => ['ru' => 'Список', 'en' => 'List', 'es' => 'Lista'],
            'route' => 'core.galleries.index',
            'icon' => 'zmdi zmdi-view-list'
        ],
        'templates' => [
            'title' => ['ru' => 'Шаблоны', 'en' => 'Templates', 'es' => 'Plantillas'],
            'route' => 'core.galleries.templates.index',
            'icon' => 'zmdi zmdi-layers'
        ]
    ]
];