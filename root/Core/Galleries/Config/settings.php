<?php
return [
    'galleries' => [
        'title' => ['ru' => 'Галереи', 'en' => 'Galleries', 'es' => 'Galerías'],
        'options' => [
            ['group_title' => ['ru' => 'Основные', 'en' => 'Main', 'es' => 'Los principales']],
            'upload_max_file_size' => [
                'title' => [
                    'ru' => 'Максимальный размер загружаемого файла',
                    'en' => 'Maximum upload size',
                    'es' => 'El tamaño máximo de subida',
                ],
                'note' => [
                    'ru' => 'Указать в МБайтах',
                    'en' => 'Specify in Mb',
                    'es' => 'Especifique en Mb'
                ],
                'type' => 'text',
                'value' => 5
            ],
            'allowed_file_extensions' => [
                'title' => [
                    'ru' => 'Поддерживаемые расширения файлов',
                    'en' => 'Supported file extensions',
                    'es' => 'Extensiones de archivo admitidos'
                ],
                'note' => [
                    'ru' => 'Если типов несколько, добавьте их через запятую',
                    'en' => 'If several types, add divided by comma',
                    'es' => 'Si varios tipos, añadir dividido por comas',
                ],
                'type' => 'text',
                'value' => 'png,jpg,gif'
            ],
            ['group_title' => ['ru' => 'Настройка каталогов', 'en' => 'Setting directories', 'es' => 'Configuración de directorios']],
            'photo_dir' => [
                'title' => [
                    'ru' => 'Каталог хранения изображений',
                    'en' => 'Catalog store images',
                    'es' => 'Almacenar imágenes del catálogo'
                ],
                'note' => [
                    'ru' => 'Корневой каталог: /public/uploads',
                    'en' => 'Root directory: /public/uploads',
                    'es' => 'Directorio raíz: /public/uploads'
                ],
                'type' => 'text',
                'value' => 'galleries'
            ],
            'thumbnail_dir' => [
                'title' => [
                    'ru' => 'Каталог хранения миниатюр изображений',
                    'en' => 'Catalog thumbnails storage',
                    'es' => 'Almacenamiento de imágenes en miniatura Catálogo',
                ],
                'note' => [
                    'ru' => 'Корневой каталог: /public/uploads',
                    'en' => 'Root directory: /public/uploads',
                    'es' => 'Directorio raíz: /public/uploads'
                ],
                'type' => 'text',
                'value' => 'galleries/thumbnail'
            ]
        ]
    ]
];