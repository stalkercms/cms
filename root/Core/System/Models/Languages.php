<?php
namespace STALKER_CMS\Core\System\Models;

use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель языки
 * Class Languages
 * @package STALKER_CMS\Vendor\Models
 */
class Languages extends BaseModel implements ModelInterface {

    use ModelTrait;
    public $timestamps = FALSE;
    protected $table = 'system_languages';
    protected $fillable = ['slug', 'title', 'iso_639_1', 'iso_639_2', 'iso_639_3', 'code', ' active', 'default', 'required'];
    protected $hidden = [];
    protected $guarded = ['id', '_method', '_token'];

    /**
     * @param $request
     * @return $this
     */
    public function insert($request) {

        $this->slug = $request::input('slug');
        $this->title = $request::input('title');
        $this->iso_639_1 = $request::input('iso_639_1');
        $this->iso_639_2 = $request::input('iso_639_2');
        $this->iso_639_3 = $request::input('iso_639_3');
        $this->code = $request::input('code');
        $this->active = $request::has('active') ? TRUE : FALSE;
        $this->default = FALSE;
        $this->required = FALSE;
        $this->save();
        return $this;
    }

    /**
     * @param $id
     * @param $request
     * @return $this
     */
    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->slug = $request::input('slug');
        $model->title = $request::input('title');
        $model->iso_639_1 = $request::input('iso_639_1');
        $model->iso_639_2 = $request::input('iso_639_2');
        $model->iso_639_3 = $request::input('iso_639_3');
        $model->code = $request::input('code');
        $model->active = $request::has('active') ? TRUE : FALSE;
        $model->default = FALSE;
        $model->required = FALSE;
        $model->save();
        $model->touch();
        return $this;
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function getDefaultStatusAttribute() {

        if($this->attributes['default']):
            return '<span class="c-green">'.array_translate(['ru' => 'Да', 'en' => 'Yes', 'es' => 'Sí']).'</span>';
        else:
            return '<span class="c-red">'.array_translate(['ru' => 'Нет', 'en' => 'No', 'es' => 'No']).'</span>';
        endif;
    }

    public function getActiveStatusAttribute() {

        if($this->attributes['active']):
            return '<span class="c-green">'.array_translate(['ru' => 'Активен', 'en' => 'Active', 'es' => 'Аctivo']).'</span>';
        else:
            return '<span class="c-red">'.array_translate(['ru' => 'Не активен', 'en' => 'No active', 'es' => 'No está activo']).'</span>';
        endif;
    }


    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['slug' => 'required', 'title' => 'required', 'iso_639_1' => 'required', 'iso_639_2' => 'required', 'iso_639_3' => 'required', 'code' => 'required'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return ['slug' => 'required', 'title' => 'required', 'iso_639_1' => 'required', 'iso_639_2' => 'required', 'iso_639_3' => 'required', 'code' => 'required'];
    }
}