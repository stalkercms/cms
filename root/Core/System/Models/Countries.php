<?php
namespace STALKER_CMS\Core\System\Models;

use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель страны
 * Class Countries
 * @package STALKER_CMS\Vendor\Models
 */
class Countries extends BaseModel implements ModelInterface {

    use ModelTrait;
    public $timestamps = FALSE;
    protected $table = 'system_countries';
    protected $fillable = ['slug', 'locale', 'title'];
    protected $hidden = [];
    protected $guarded = ['id', '_method', '_token'];

    /**
     * @param $request
     * @return $this
     */
    public function insert($request) {

        $this->slug = $request::input('slug');
        $this->locale = \App::getLocale();
        $this->title = $request::input('title');
        $this->save();
        return $this;
    }

    /**
     * @param $id
     * @param $request
     * @return $this
     */
    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->slug = $request::input('slug');
        $model->title = $request::input('title');
        $model->save();
        $model->touch();
        return $this;
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['slug' => 'required', 'title' => 'required'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return ['slug' => 'required', 'title' => 'required'];
    }
}