<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

/**
 * Контроллер создания боковой панели меню
 * Class SideBarController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class SideBarController extends ModuleController {

    /**
     * Строит боковое меню панели управления
     * @param $package_name
     * @param array $package_permissions
     * @param array $menu
     * @return array
     */
    public static function getSidebarMenu($package_name, array $package_permissions, array $menu = []) {

        $menu = [];
        if($_menu = config("$package_name::menu")):
            $_menu['permit'] = FALSE;
            if(!empty($_menu['menu_child'])):
                if(is_string($_menu['menu_child'])):
                    $_menu['menu_child'] = (new $_menu['menu_child']())->make(\App::getLocale());
                endif;
                foreach($_menu['menu_child'] as $action => $action_info):
                    if(!isset($_menu['menu_child'][$action]['permit'])):
                        $_menu['menu_child'][$action]['permit'] = FALSE;
                    endif;
                    if(in_array($action, $package_permissions)):
                        $_menu['menu_child'][$action]['permit'] = TRUE;
                        $_menu['permit'] = TRUE;
                    endif;
                endforeach;
            else:
                if($segments = explode('_', $package_name, 2)):
                    $module_name = $segments[count($segments) - 1];
                    if(in_array($module_name, $package_permissions)):
                        $_menu['permit'] = TRUE;
                    endif;
                endif;
            endif;
            $menu[] = $_menu;
        endif;
        return $menu;
    }

    /**
     * Проверяет наличие активного подменю
     * @param array $child_menu
     * @return bool
     */
    public static function menuChildActive(array $child_menu) {

        if(!empty($child_menu)):
            foreach($child_menu as $child_name => $child_module):
                if(self::isLinkActive($child_module['route'])):
                    return TRUE;
                endif;
            endforeach;
        endif;
        return FALSE;
    }

    /**
     * Проверяет является ли роут активным
     * @param $routeName
     * @return bool
     */
    public static function isLinkActive($routeName) {

        if(!empty($routeName)):
            if(\Route::currentRouteName() == $routeName):
                return TRUE;
            else:
                if(static::getMainRoute() == $routeName):
                    return TRUE;
                endif;
            endif;
        endif;
        return FALSE;
    }

    /**
     * Проверяет является ли роут основным (index)
     * @param null $currentRouteName
     * @return bool|string
     */
    private static function getMainRoute($currentRouteName = NULL) {

        if(is_null($currentRouteName)):
            $currentRouteName = \Route::currentRouteName();
        endif;
        if($route_segments = explode('.', $currentRouteName)):
            $route_segments[count($route_segments) - 1] = 'index';
            $mainRoute = implode('.', $route_segments);
            if(\Route::has($mainRoute)):
                return $mainRoute;
            endif;
        endif;
        return FALSE;
    }
}