<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use STALKER_CMS\Core\System\Models\Languages;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

/**
 * Контроллер языков
 * Class LanguagesController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class LanguagesController extends ModuleController implements CrudInterface {

    protected $model;

    /**
     * LanguagesController constructor.
     */
    public function __construct() {

        $this->model = new Languages();
        \PermissionsController::allowPermission('core_system', 'languages');
        $this->middleware('auth');
    }

    /**
     * Список языков
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $languages = $this->model->paginate(25);
        return view('core_system_views::languages.index', compact('languages'));
    }

    /**
     * Добавление языка
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        return view('core_system_views::languages.create');
    }

    /**
     * Сохранение языка
     * @return mixed
     */
    public function store() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            $lang_directory = base_path('home/Resources/Lang/'.$request::input('slug'));
            if(\File::exists($lang_directory) === FALSE):
                \File::makeDirectory($lang_directory, 0754, TRUE);
                \File::put($lang_directory.'/.gitkeep', '');
            endif;
            $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('core.system.languages.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Редактирование языка
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id) {

        \PermissionsController::allowPermission('core_system', 'languages');
        $language = $this->model->findOrFail($id);
        return view('core_system_views::languages.edit', compact('language'));
    }

    /**
     * Обновление языка
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update($id) {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->replace($id, $request);
            return \ResponseController::success(200)->redirect(route('core.system.languages.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Удаление языка
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \RequestController::isAJAX()->init();
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('core.system.languages.index'))->json();
    }
}