<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use STALKER_CMS\Core\System\Models\Countries;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

/**
 * Контроллер Стран
 * Class UsersController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class CountriesController extends ModuleController implements CrudInterface {

    protected $model;

    /**
     * CountriesController constructor.
     * @param Countries $country
     */
    public function __construct(Countries $country) {

        $this->model = $country;
        \PermissionsController::allowPermission('core_system', 'countries');
        $this->middleware('auth');
    }

    /**
     * Список стран
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $request = \RequestController::init();
        $countries = $this->model->whereLocale(\App::getLocale());
        if($request::has('search')):
            $search = $request::get('search');
            $countries = $countries->where(function($query) use ($search) {

                $query->orWhere('title', 'like', '%'.$search.'%');
            });
        endif;
        $countries = $countries->orderBy('title')->paginate(25);
        return view('core_system_views::countries.index', compact('countries'));
    }

    /**
     * Добавление страны
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        return view('core_system_views::countries.create');
    }

    /**
     * Сохранение страны
     * @return mixed
     */
    public function store() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('core.system.countries.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Редактирование страны
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id) {


        $country = $this->model->findOrFail($id);
        return view('core_system_views::countries.edit', compact('country'));
    }

    /**
     * Обновление страны
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update($id) {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->replace($id, $request);
            return \ResponseController::success(200)->redirect(route('core.system.countries.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Удаление страны
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \RequestController::isAJAX()->init();
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('core.system.countries.index'))->json();
    }

    /*****************************************************************************/
    public function import() {

        $request = \RequestController::init();
        if($request::hasFile('import') && $request::file('import')->isValid()):
            if($csv_records = csv_to_array($request::file('import')->getRealPath())):
                foreach($csv_records as $index => $record):
                    foreach($record as $value):
                        $slug = transliteration($value);
                        if($this->model->whereLocale(\App::getLocale())->whereSlug($slug)->exists() === FALSE):
                            Countries::create(['slug' => $slug, 'locale' => \App::getLocale(), 'title' => $value]);
                        endif;
                    endforeach;
                endforeach;
            endif;
        endif;
        return redirect()->route('core.system.countries.index');
    }
}