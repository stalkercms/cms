@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::dashboard.title')</h2>
        <ul class="actions">
            @if(\PermissionsController::isPackageEnabled('core_content'))
                <li>
                    <a target="_blank" href="@route('public.page.index')">
                        <i class="zmdi zmdi-home"></i>
                    </a>
                </li>
            @endif
            <li class="dropdown">
                <a href="" data-toggle="dropdown">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a href="@route('dashboard')" class="edit-dashboard-template">
                            @lang('core_system_lang::dashboard.edit_template')
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="card">
        <div class="card-body">
            <section id="dashboard-container">{!! $dashboard_content !!}</section>
        </div>
    </div>
@stop
@section('scripts_after')
    {!! Html::script('core/ace/ace.js') !!}
    <script>
        $(function () {
            var editor = {};
            function initEditor(html_content, button) {
                editor = ace.edit("dashboard-container");
                editor.setTheme("ace/theme/monokai");
                editor.getSession().setMode("ace/mode/javascript");
                editor.getSession().setUseSoftTabs(true);
                document.getElementById('dashboard-container').style.fontSize = '14px';
                editor.getSession().setUseWrapMode(true);
                editor.setShowPrintMargin(false);
                editor.setOptions({
                    maxLines: Infinity
                });
                editor.setValue(html_content);
                $("#dashboard-container").after(button);
            }
            $(".edit-dashboard-template").click(function (event) {
                event.preventDefault();
                $.ajax({
                    url: "{!! route('core.dashboard.template.edit') !!}",
                    method: 'POST',
                    dataType: 'json',
                    beforeSend: function (xhr) {},
                    success: function (response) {
                        if (response.status) {
                            initEditor(response.html, response.button);
                            $(".edit-dashboard-template").parents('ul.actions').remove();
                        } else {
                            BASIC.notify(null, response.errorText, 'bottom', 'center', null, 'warning', 'animated flipInX', 'animated flipOutX');
                        }
                        if (response.redirectURL !== false) {
                            window.location.href = response.redirectURL;
                        }
                    }
                });
            });
            $(document).on('click', "#dashboard-template-save", function () {
                $.ajax({
                    url: "{!! route('core.dashboard.template.update') !!}",
                    method: 'POST',
                    dataType: 'json',
                    data: {content: editor.getValue()},
                    beforeSend: function () {
                        $("#dashboard-template-save").elementDisabled(true);
                    },
                    success: function (response) {
                        $("#dashboard-template-save").elementDisabled(false);
                        if (response.status) {
                            BASIC.notify(null, response.responseText, 'bottom', 'center', null, 'success', 'animated flipInX', 'animated flipOutX');
                        } else {
                            BASIC.notify(null, response.errorText, 'bottom', 'center', null, 'warning', 'animated flipInX', 'animated flipOutX');
                        }
                        if (response.redirectURL !== false) {
                            window.location.href = response.redirectURL;
                        }
                    }
                });
            });
        });
    </script>
@stop