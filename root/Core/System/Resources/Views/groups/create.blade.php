@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! array_translate(config('core_system::menu.title')) !!}
        </li>
        <li>
            <a href="{!! route('core.system.groups.index') !!}">
                <i class="{{ config('core_system::menu.menu_child.groups.icon') }}"></i> {!! array_translate(config('core_system::menu.menu_child.groups.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-plus"></i> @lang('core_system_lang::groups.insert.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-plus"></i> @lang('core_system_lang::groups.insert.title')</h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                <div class="col-sm-6">
                    {!! Form::open(['route' => 'core.system.groups.store', 'class' => 'form-validate', 'id' => 'add-group-form']) !!}
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('core_system_lang::groups.insert.form.title')</label>
                        </div>
                        <small class="help-description">
                            @lang('core_system_lang::groups.insert.form.title_help_description')
                        </small>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('slug', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('core_system_lang::groups.insert.form.slug')</label>
                        </div>
                        <small class="help-description">
                            @lang('core_system_lang::groups.insert.form.slug_help_description')
                        </small>
                    </div>
                    <div class="checkbox m-b-15">
                        <label>
                            {!! Form::checkbox('new_interface', NULL, FALSE, ['id' => 'js-get-new-interface', 'autocomplete' => 'off']) !!}
                            <i class="input-helper"></i>
                            @lang('core_system_lang::groups.insert.form.new_interface')
                        </label>
                        <small class="help-description">
                            @lang('core_system_lang::groups.insert.form.new_interface_help_description')
                        </small>
                        <div class="clearfix"></div>
                        <code class="hidden" id="js-set-new-interface"></code>
                    </div>
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('core_system_lang::groups.insert.form.submit')</span>
                    </button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
    <script>
        $(function () {
            $("#js-get-new-interface").change(function () {
                var translate = {
                    "path": "@lang('core_system_lang::groups.insert.javascript.new_interface_path')",
                    "error": "@lang('core_system_lang::groups.insert.javascript.new_interface_error')"
                }
                if ($(this).prop('checked')) {
                    if ($("input[name='slug']").val() != '') {
                        $("#js-set-new-interface").removeClass('hidden').html(translate['path'] + ': /home/resources/views/' + $("input[name='slug']").val());
                    } else {
                        $(this).removeAttr('checked');
                        $("#js-set-new-interface").removeClass('hidden').html(translate['error']);
                    }
                } else {
                    $("#js-set-new-interface").addClass('hidden').html('');
                }
            });
        })
    </script>
@stop