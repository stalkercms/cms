@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! array_translate(config('core_system::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_system::menu.menu_child.groups.icon') }}"></i> {!! array_translate(config('core_system::menu.menu_child.groups.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_system::menu.menu_child.groups.icon') }}"></i>
            {!! array_translate(config('core_system::menu.menu_child.groups.title')) !!}
        </h2>
    </div>
    @BtnAdd('core.system.groups.create')
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('core_system_lang::groups.list')
                </div>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($groups as $index => $group)
                    <div class="js-item-container list-group-item media">
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a href="" data-toggle="dropdown" aria-expanded="true">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{!! route('core.system.groups.edit', $group->id) !!}">
                                            @lang('core_system_lang::groups.edit')
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{!! route('core.system.groups.accesses-index', $group->id) !!}">
                                            @lang('core_system_lang::groups.permissions')
                                        </a>
                                    </li>
                                    @if($group->id == Auth::user()->group_id || $group->required)
                                    @else
                                        <li class="divider"></li>
                                        <li>
                                            <a class="c-red js-item-remove" href="">
                                                @lang('core_system_lang::groups.delete.submit')
                                            </a>
                                            {!! Form::open(['route' => ['core.system.groups.destroy', $group->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                            <button type="submit"
                                                    data-question="@lang('core_system_lang::groups.delete.question') &laquo;{{ $group->title }}&raquo;?"
                                                    data-confirmbuttontext="@lang('core_system_lang::groups.delete.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('core_system_lang::groups.delete.cancelbuttontext')">
                                                @lang('core_system_lang::groups.delete.submit')
                                            </button>
                                            {!! Form::close() !!}
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">{!! $group->title !!}</div>
                            <small class="lgi-text">
                                {!! $group->users_count() !!}
                                @choice(trans('core_system_lang::groups.users_count'), $group->users_count())
                            </small>
                            <ul class="lgi-attrs">
                                <li>ID: @numDimensions($group->id)</li>
                                <li>
                                    @lang('core_system_lang::groups.symbolic_code'):
                                    {!! $group->slug !!}
                                </li>
                            </ul>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('core_system_lang::groups.empty')</h2>
                @endforelse
            </div>
        </div>
    </div>
@stop