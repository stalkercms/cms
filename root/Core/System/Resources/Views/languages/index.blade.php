@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! array_translate(config('core_system::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_system::menu.menu_child.languages.icon') }}"></i> {!! array_translate(config('core_system::menu.menu_child.languages.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_system::menu.menu_child.languages.icon') }}"></i>
            {!! array_translate(config('core_system::menu.menu_child.languages.title')) !!}
        </h2>
    </div>
    @BtnAdd('core.system.languages.create')
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                @if($languages->count())
                    <div class="ah-label hidden-xs">
                        @lang('core_system_lang::languages.list')
                    </div>
                @endif
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($languages as $language)
                    <div class="js-item-container list-group-item media">
                        @if(\File::exists(public_path('core/images/languages/'.$language->slug.'.png')))
                            <div class="pull-left clearfix">
                                <img alt="{{ $language->title }}" class="lgi-img" src="{!! asset('core/images/languages/'.$language->slug.'.png') !!}">
                            </div>
                        @endif
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a href="" data-toggle="dropdown" aria-expanded="true">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{!! route('core.system.languages.edit', $language->id) !!}">
                                            @lang('core_system_lang::countries.edit')
                                        </a>
                                    </li>
                                    @if(!$language->required)
                                        <li class="divider"></li>
                                        <li>
                                            <a class="c-red js-item-remove" href="">
                                                @lang('core_system_lang::languages.delete.submit')
                                            </a>
                                            {!! Form::open(['route' => ['core.system.languages.destroy', $language->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                            <button type="submit"
                                                    data-question="@lang('core_system_lang::languages.delete.question') &laquo;{{ $language->title }}&raquo;?"
                                                    data-confirmbuttontext="@lang('core_system_lang::languages.delete.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('core_system_lang::languages.delete.cancelbuttontext')">
                                            </button>
                                            {!! Form::close() !!}
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">{!! $language->title !!}</div>
                            <ul class="lgi-attrs">
                                <li>
                                    @lang('core_system_lang::languages.slug'):
                                    {!! $language->slug !!}
                                </li>
                                <li>
                                    @lang('core_system_lang::languages.default'):
                                    {!! $language->default_status !!}
                                </li>
                                <li>
                                    @lang('core_system_lang::languages.active'):
                                    {!! $language->active_status !!}
                                </li>
                            </ul>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('core_system_lang::languages.empty')</h2>
                @endforelse
                <div class="lg-pagination p-10">
                    {!! $languages->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop