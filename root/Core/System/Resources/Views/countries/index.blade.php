@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! array_translate(config('core_system::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_system::menu.menu_child.countries.icon') }}"></i> {!! array_translate(config('core_system::menu.menu_child.countries.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_system::menu.menu_child.countries.icon') }}"></i>
            {!! array_translate(config('core_system::menu.menu_child.countries.title')) !!}
        </h2>
    </div>
    @BtnAdd('core.system.countries.create')
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('core_system_lang::countries.list')
                </div>
                {!! Form::open(['route' => 'core.system.countries.index', 'method' => 'get']) !!}
                <div class="ah-search">
                    <input type="text" name="search" placeholder="@lang('core_system_lang::countries.search')" class="ahs-input">
                    <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                </div>
                {!! Form::close() !!}
                <ul class="actions">
                    <li>
                        <a href="" data-ma-action="action-header-open">
                            <i class="zmdi zmdi-search"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" href="" aria-expanded="false">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="" class="js-import-elements">
                                    @lang('core_system_lang::countries.import')
                                </a>
                                {!! Form::open(['route' => ['core.system.countries.import'], 'class' => 'hidden', 'files' => TRUE]) !!}
                                {!! Form::file('import', ['accept' => '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel']) !!}
                                {!! Form::close() !!}
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($countries as $country)
                    <div class="js-item-container list-group-item media">
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a href="" data-toggle="dropdown" aria-expanded="true">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{!! route('core.system.countries.edit', $country->id) !!}">
                                            @lang('core_system_lang::countries.edit')
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a class="c-red js-item-remove" href="">
                                            @lang('core_system_lang::countries.delete.submit')
                                        </a>
                                        {!! Form::open(['route' => ['core.system.countries.destroy', $country->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                        <button type="submit"
                                                data-question="@lang('core_system_lang::countries.delete.question') &laquo;{{ $country->title }}&raquo;?"
                                                data-confirmbuttontext="@lang('core_system_lang::countries.delete.confirmbuttontext')"
                                                data-cancelbuttontext="@lang('core_system_lang::countries.delete.cancelbuttontext')">
                                        </button>
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">{!! $country->title !!}</div>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('core_system_lang::countries.empty')</h2>
                @endforelse
                <div class="lg-pagination p-10">
                    {!! $countries->appends(\Request::only('search'))->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop