<aside id="sidebar" class="sidebar c-overflow">
    <div class="s-profile">
        <a class="profile-menu-toggle" href="" data-ma-action="profile-menu-toggle"
           @if(File::exists(public_path('theme/images/sidebar-logo.png')))style="background-image: url('{!! asset('theme/images/sidebar-logo.png') !!}')"@endif>
            <div class="sp-pic">
                @ProfileAvatar(Auth::user()->name, Auth::user()->avatar_thumbnail)
            </div>
            <div class="sp-info">
                {!! Auth::user()->name !!}
                <i class="zmdi zmdi-caret-down"></i>
            </div>
        </a>
        <ul class="main-menu">
            @if(\PermissionsController::allowPermission('core_system', 'users', FALSE))
                <li>
                    <a href="{!! route('core.system.users.edit', Auth::user()->id) !!}">
                        <i class="zmdi zmdi-account"></i> @lang('core_system_lang::dashboard.edit_profile')
                    </a>
                </li>
            @endif
            @if(\PermissionsController::allowPermission('core_system', 'groups', FALSE))
                <li>
                    <a href="{!! route('core.system.groups.accesses-index', Auth::user()->group_id) !!}">
                        <i class="zmdi zmdi-lock-open"></i> @lang('core_system_lang::dashboard.edit_profile_permissions')
                    </a>
                </li>
            @endif
        </ul>
    </div>
    @include('core_system_views::assets.sidebar')
</aside>