@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="active">
            <i class="{{ config('core_system::menu.menu_child.settings.icon') }}"></i> @lang('core_system_lang::settings.main.title')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_system::menu.menu_child.settings.icon') }}"></i>
            @lang('core_system_lang::settings.main.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            {!! Form::open(['url' => URL::route('cms.settings.update'), 'class' => 'form-validate', 'id' => 'main-settings-form']) !!}
            <div class="row">
                <div class="col-sm-6">
                    <p class="f-500 m-b-15 c-black">
                        <i class="zmdi zmdi-translate"></i> @lang('core_system_lang::settings.main.interface_language')
                    </p>
                    <div class="form-group p-l-0">
                        {!! Form::select('APP_LOCALE', $languages, \App::getLocale(), ['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="clearfix"></div>
                    <p class="f-500 m-b-15 c-black">
                        <i class="zmdi zmdi-translate"></i> @lang('core_system_lang::settings.main.interface_theme')
                    </p>
                    <div class="form-group p-l-0">
                        {!! Form::select('CORE_THEME', ['light' => 'Light Theme', 'dark' => 'Dark Theme'], config('app.core_theme'), ['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('APPLICATION_NAME', config('app.application_name'), ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('core_system_lang::settings.main.app_name')</label>
                        </div>
                    </div>
                    @if($CONFIG_CACHED)
                        <div class="clearfix">
                            <small class="m-b-10 m-t-5 c-red f-10">
                                <i class="zmdi zmdi-alert-circle-o"></i>
                                @lang('core_system_lang::settings.main.clear_cache_part1')
                                <a href="{!! route('core.system.settings.index') !!}">
                                    @lang('core_system_lang::settings.main.clear_cache_part2')
                                </a>
                            </small>
                        </div>
                    @endif
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="f-100 c-gray m-b-10">@lang('core_system_lang::settings.main.logo')</p>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn btn-primary btn-file m-r-10 waves-effect">
                                    <span class="fileinput-new">@lang('core_system_lang::settings.main.logo_select')</span>
                                    <span class="fileinput-exists">@lang('core_system_lang::settings.main.logo_change')</span>
                                    {!! Form::file('logo', ['accept' => "image/png, image/x-png"]) !!}
                                </span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <small class="m-b-10 m-t-5 c-gray">@lang('core_system_lang::settings.main.favicon_help_description')</small>
                        </div>
                    </div>
                    @if(!class_exists('Imagick'))
                    <span class="badge bgm-red c-white m-t-15 f-14 p-5">Imagick not installed</span>
                    @else
                        <div class="row m-t-20">
                            <div class="col-sm-12">
                                <p class="f-100 c-gray m-b-10">@lang('core_system_lang::settings.main.favicon')</p>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn btn-primary btn-file m-r-10 waves-effect">
                                    <span class="fileinput-new">@lang('core_system_lang::settings.main.favicon_select')</span>
                                    <span class="fileinput-exists">@lang('core_system_lang::settings.main.favicon_change')</span>
                                    {!! Form::file('favicon', ['accept' => "image/png, image/x-png"]) !!}
                                </span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <small class="m-b-10 m-t-5 c-gray">@lang('core_system_lang::settings.main.favicon_help_description')</small>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('core_system_lang::settings.main.submit')</span>
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop