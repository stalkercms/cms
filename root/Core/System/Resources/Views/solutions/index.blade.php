@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! array_translate(config('core_system::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_system::menu.menu_child.modules_solutions.icon') }}"></i> {!! array_translate(config('core_system::menu.menu_child.modules_solutions.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_system::menu.menu_child.modules_solutions.icon') }}"></i>
            {!! array_translate(config('core_system::menu.menu_child.modules_solutions.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div role="tabpanel" class="tab">
            <div class="card-header ch-alt m-b-20">
                <ul class="tab-nav" role="tablist" data-tab-color="teal">
                    <?php $tab_index = 0; ?>
                    @foreach($solutions_packages as $symbolic_code => $lists)
                        <li class="{!! !$tab_index++ ? 'active' : '' !!}">
                            <a href="#module_{!! $symbolic_code !!}" aria-controls="module_{!! $symbolic_code !!}"
                               role="tab" data-toggle="tab">
                                {!! array_translate($lists['title']) !!}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="card-body card-padding p-t-0">
                <div class="row">
                    <div class="tab-content p-0">
                        <?php $tab_index = 0; ?>
                        @foreach($solutions_packages as $symbolic_code => $lists)
                            <div role="tabpanel" id="module_{!! $symbolic_code !!}" class="tab-pane {!! !$tab_index++ ? 'active in' : '' !!} animated fadeIn">
                                <?php $index = 1; ?>
                                @forelse($lists['solutions'] as $package_name => $package)
                                    <div class="col-sm-4">
                                        <div class="card">
                                            <div class="card-header bgm-bluegray">
                                                <h2 class="f-14">
                                                    {{ array_translate($package['package_title']) }}
                                                </h2>
                                                <div class="actions cursor-pointer">
                                                    <a title="@lang('core_system_lang::solutions.install')"
                                                       data-action="{{ route('core.system.modules.solutions.enable') }}"
                                                       class="confirm-warning solution-install"
                                                       data-alert-type="info" autocomplete="off" data-method="post"
                                                       data-params="section={!! $symbolic_code !!}&solution={!! $package['package_name'] !!}"
                                                       data-question="@lang('core_system_lang::solutions.enable.question') &laquo;{{ array_translate($package['package_title']) }}&raquo;?"
                                                       data-confirmbuttontext="@lang('core_system_lang::solutions.enable.confirmbuttontext')"
                                                       data-cancelbuttontext="@lang('core_system_lang::solutions.enable.cancelbuttontext')">
                                                        <i class="zmdi zmdi-download c-white"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="card-body card-padding m-h-100 p-t-5">
                                                <small>{!! array_translate($package['package_description']) !!}</small>
                                                <br><br>
                                                {{ array_translate(['ru' => 'Версия', 'en'=>'Version', 'es'=>'Versión']) }}:
                                                {!! number_format($package['version']['ver'], 1) !!}
                                                {!! array_translate(['ru' => 'от', 'en'=>'from', 'es'=>'de']) !!}:
                                                {!! $package['version']['date'] !!}
                                            </div>
                                        </div>
                                    </div>
                                    @if($index % 3 == 0)
                                        <div class="clearfix"></div>
                                    @endif
                                    <?php $index++;?>
                                @empty
                                    <h2 class="p-l-30 f-16 c-gray">@lang('core_system_lang::solutions.empty')</h2>
                                @endforelse
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop