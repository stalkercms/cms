<?php
return [
    'cache_settings' => 'Configuración de la caché',
    'cache_clear' => 'Configuración de la caché borrar',
    'php_info' => 'Información PHP',
    'main' => [
        'title' => 'Ajustes',
        'interface_language' => 'Idioma de interfaz',
        'interface_theme' => 'El tema de la interfaz',
        'app_name' => 'Nombre de la aplicación',
        'clear_cache_part1' => 'Después de cambiar la configuración de la aplicación, borrar la memoria caché',
        'clear_cache_part2' => 'ajustes',
        'logo' => 'Aplicaciones logotipo',
        'logo_help_description' => 'Apoyado único formato png <br> Dimensiones recomendadas 412x214 px',
        'logo_select' => 'Selecto',
        'logo_change' => 'Enmendar',
        'logo_delete' => 'Eliminar',
        'favicon' => 'Aplicaciones favicon',
        'favicon_help_description' => 'Apoyado único formato PNG',
        'favicon_select' => 'Selecto',
        'favicon_change' => 'Enmendar',
        'submit' => 'Guardar'
    ]
];