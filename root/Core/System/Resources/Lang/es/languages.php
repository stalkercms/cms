<?php
return [
    'list' => 'La lista de idiomas disponibles',
    'edit' => 'Editar',
    'empty' => 'Lista está vacía',
    'slug' => 'Simbólico código',
    'default' => 'Idioma principal',
    'active' => 'Estatus',
    'delete' => [
        'question' => 'Eliminar el lenguaje',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar'
    ],
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Agregando la lengua',
        'form' => [
            'title' => 'Título',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: ru',
            'code' => 'Código',
            'active' => 'activo',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Idioma de edición',
        'form' => [
            'title' => 'Título',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: ru',
            'code' => 'Código',
            'active' => 'Activo',
            'submit' => 'Guardar'
        ]
    ]
];