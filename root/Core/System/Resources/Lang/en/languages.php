<?php
return [
    'list' => 'The list of available languages',
    'edit' => 'Edit',
    'empty' => 'List is empty',
    'slug' => 'Symbolic code',
    'default' => 'Primary',
    'active' => 'Status',
    'delete' => [
        'question' => 'Delete language',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete'
    ],
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding language',
        'form' => [
            'title' => 'Title',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: ru',
            'code' => 'Code',
            'active' => 'Active',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Editing language',
        'form' => [
            'title' => 'Title',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: ru',
            'code' => 'Code',
            'active' => 'Active',
            'submit' => 'Save'
        ]
    ]
];