<?php
return [
    'list' => 'List of countries',
    'search' => 'Enter the name of the country',
    'edit' => 'Edit',
    'empty' => 'List is empty',
    'import' => 'Import list',
    'delete' => [
        'question' => 'Delete country',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding a country',
        'form' => [
            'title' => 'Title',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: russia',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Editing the country',
        'form' => [
            'title' => 'Title',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: russia',
            'submit' => 'Save'
        ]
    ]
];