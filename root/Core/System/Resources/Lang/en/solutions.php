<?php
return [
    'settings' => 'Settings',
    'check_updates' => 'Check for updates',
    'install' => 'Install',
    'empty' => 'Has no available solutions',
    'disable' => [
        'question' => 'Remove solution',
        'confirmbuttontext' => 'Yes, remove',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Remove',
    ],
    'enable' => [
        'question' => 'Install solution',
        'confirmbuttontext' => 'Yes, install',
        'cancelbuttontext' => 'No, I change my mind'
    ]
];