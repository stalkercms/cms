<?php
return [
    'list' => 'List of cities',
    'search' => 'Enter the name of the city',
    'edit' => 'Edit',
    'empty' => 'List is empty',
    'import' => 'Import list',
    'delete' => [
        'question' => 'Delete city',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete'
    ],
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding a city',
        'form' => [
            'title' => 'Title',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: moscos',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Editing the city',
        'form' => [
            'title' => 'Title',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: moscos',
            'submit' => 'Save'
        ]
    ]
];