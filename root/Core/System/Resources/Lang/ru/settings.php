<?php
return [
    'cache_settings' => 'Кэшировать настройки',
    'cache_clear' => 'Очистить кэш настроек',
    'php_info' => 'Информация PHP',
    'main' => [
        'title' => 'Настройки',
        'interface_language' => 'Язык интерфейса',
        'interface_theme' => 'Тема интерфейса',
        'app_name' => 'Название приложение',
        'clear_cache_part1' => 'После изменения настроек приложения очистите кэш',
        'clear_cache_part2' => 'настроек',
        'logo' => 'Лого приложения',
        'logo_help_description' => 'Поддерживается только формат png <br> Рекомендуемые размеры 412х214 px',
        'logo_select' => 'Выбрать',
        'logo_change' => 'Изменить',
        'logo_delete' => 'Удалить',
        'favicon' => 'Favicon приложения',
        'favicon_help_description' => 'Поддерживается только формат PNG',
        'favicon_select' => 'Выбрать',
        'favicon_change' => 'Изменить',
        'submit' => 'Сохранить'
    ]
];