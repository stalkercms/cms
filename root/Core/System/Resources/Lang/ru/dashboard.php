<?php
return [
    'control_panel' => 'Панель управления',
    'edit_profile' => 'Редактировать профиль',
    'edit_profile_permissions' => 'Настройка доступов',
    'history' => [
        'title' => 'Последние изменения',
        'empty' => 'Список пустой',
        'updated' => 'Дата обновления',
        'author' => 'Автор'
    ],
    'header' => [
        'notification' => 'Уведомления',
        'notification_view_all' => 'Читать все',
        'settings' => [
            'full_screen' => 'Полный экран',
            'edit' => 'Настройка',
            'logout' => 'Завершить сеанс',
        ],
        'debug_mode' => 'Режим отладки',
        'services_mode' => 'Режим обслуживания',
        'config_cached' => 'Настройки модулей закэшированы',
        'config_no_cached' => 'Настройки модулей не закэшированы'
    ],
    'title' => 'Дашборд',
    'feedback' => [
        'title' => 'Обратная связь',
        'inbox' => 'Получено',
        'views' => 'Просмотрено',
        'sends' => 'Отвечено',
    ],
    'feedback_list' => [
        'title' => 'Обратная связь',
        'show_all' => 'Читать все'
    ],
    'edit_template' => 'Изменить шаблон'
];