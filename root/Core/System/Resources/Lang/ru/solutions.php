<?php
return [
    'settings' => 'Настройка',
    'check_updates' => 'Проверить обновления',
    'install' => 'Установить',
    'empty' => 'Нет доступных решений',
    'disable' => [
        'question' => 'Удалить решение',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить'
    ],
    'enable' => [
        'question' => 'Установить решение',
        'confirmbuttontext' => 'Да, установить',
        'cancelbuttontext' => 'Нет, я передумал'
    ]
];