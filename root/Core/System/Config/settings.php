<?php
return [
    'settings' => [
        'title' => ['ru' => 'Система', 'en' => 'System', 'es' => 'Sistema'],
        'options' => [
            ['group_title' => ['ru' => 'Основные', 'en' => 'Main', 'es' => 'Los principales']],
            'base_locale' => [
                'title' => ['ru' => 'Основной язык системы', 'en' => 'The main system language', 'es' => 'El idioma principal del sistema'],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => env('APP_LOCALE', 'ru')
            ],
            'php' => [
                'title' => ['ru' => 'Путь к php', 'en' => 'The path to the php', 'es' => 'La ruta de acceso al php'],
                'note' => [
                    'ru' => 'Укажите полный путь к php (не обязательно)',
                    'en' => 'Specify the full path to the php (not necessary)',
                    'es' => 'Especificar la ruta completa al php (no necesariamente)'
                ],
                'type' => 'text',
                'value' => ''
            ],
            'composer' => [
                'title' => ['ru' => 'Путь к composer', 'en' => 'The path to the composer', 'es' => 'La ruta de acceso al compositor'],
                'note' => [
                    'ru' => 'Укажите полный путь к файлу',
                    'en' => 'Specify the full path to the file',
                    'es' => 'Especificar la ruta completa al archivo'
                ],
                'type' => 'text',
                'value' => '/usr/local/bin/composer'
            ],
            'services_mode' => [
                'title' => ['ru' => 'Режим обслуживания', 'en' => 'Maintenance mode', 'es' => 'Modo de mantenimiento'],
                'note' => [
                    'ru' => 'Позволяет «отключать» приложение, в момент обновления',
                    'en' => 'It allows you to "turn off" the application, at the time of renovation',
                    'es' => 'Le permite "apagar" la aplicación, en el momento de la renovación'
                ],
                'type' => 'checkbox',
                'value' => ''
            ],
            'debug_mode' => [
                'title' => ['ru' => 'Режим отладки', 'en' => 'Debug mode', 'es' => 'Modo de depuración'],
                'note' => [
                    'ru' => 'Позволяет включать режим отладки для анализа ошибок',
                    'en' => 'It allows you to enable debug mode for error analysis',
                    'es' => 'Se le permite activar el modo de depuración para el análisis de errores'
                ],
                'type' => 'checkbox',
                'value' => env('APP_DEBUG', FALSE)
            ]
        ]
    ],
    'users' => [
        'title' => ['ru' => 'Пользователи', 'en' => 'Users', 'es' => 'Usuarios'],
        'options' => [
            ['group_title' => ['ru' => 'Основные', 'en' => 'Los principales', 'es' => 'Principal']],
            'avatar_dir' => [
                'title' => [
                    'ru' => 'Каталог хранения аватаров',
                    'en' => 'Catalog storage avatars',
                    'es' => 'Avatares almacenamiento catálogo'
                ],
                'note' => [
                    'ru' => 'Корневой каталог: /public/uploads',
                    'en' => 'Root directory: /public/uploads',
                    'es' => 'Directorio raíz: /public/uploads'
                ],
                'type' => 'text',
                'value' => 'avatars'
            ]
        ]
    ]
];