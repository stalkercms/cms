<?php
namespace STALKER_CMS\Core\OpenGraph\Providers;

use Illuminate\Support\Str;
use STALKER_CMS\Core\OpenGraph\Http\Controllers\PublicOpenGraphController;
use STALKER_CMS\Vendor\Providers\ServiceProvider;

/**
 * Class ModuleServiceProvider
 * @package STALKER_CMS\Core\OpenGraph\Providers
 */
class ModuleServiceProvider extends ServiceProvider {

    /**
     * Метод загрузки
     */
    public function boot() {

        $this->setPath(__DIR__.'/../');
        $this->registerViews('core_open_graph_views');
        $this->registerLocalization('core_open_graph_lang');
        $this->registerConfig('core_open_graph::config', 'Config/open_graph.php');
        $this->registerSettings('core_open_graph::settings', 'Config/settings.php');
        $this->registerActions('core_open_graph::actions', 'Config/actions.php');
        $this->registerSystemMenu('core_open_graph::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
    }

    /**
     * Метод регистрации
     */
    public function register() {

        \App::bind('PublicOpenGraphController', function() {

            return new PublicOpenGraphController();
        });
    }

    /********************************************************************************************************************/
    /**
     * Регистрация blade директив
     */
    public function registerBladeDirectives() {

        \Blade::directive('OpenGraph', function($expression) {

            if(Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            $html = "<?php \$__env->startSection('OpenGraph'); ?>";
            if(empty($expression)):
                $html .= '<?php $openGraph = json_decode($page->open_graph, TRUE); ?>';
            else:
                $html .= '<?php $openGraph = json_decode('.$expression.'->open_graph, TRUE); ?>';
            endif;
            $html .= '<meta property="og:title" content="<?php echo e(@$openGraph[\'og:title\']); ?>">'."\n";
            $html .= "\t".'<meta property="og:description" content="<?php echo e(@$openGraph[\'og:description\']); ?>">'."\n";
            $html .= '<?php if(!empty($openGraph["og:image"])): ?>';
            $html .= '<?php foreach($openGraph["og:image"] as $image): ?>';
            $html .= "\t".'<meta property="og:image" content="<?php echo asset(\'uploads/open_graph/\' . $image); ?>">'."\n";
            $html .= '<?php endforeach; ?>';
            $html .= '<?php endif; ?>';
            $html .= "<?php \$__env->stopSection(); ?>";
            return $html;
        });
    }
}