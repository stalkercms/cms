<?php
namespace STALKER_CMS\Core\OpenGraph\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Фасад контроллера OpenGraph
 * Class OpenGraph
 * @package STALKER_CMS\Core\OpenGraph\Facades
 */
class OpenGraph extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicOpenGraphController';
    }
}