<?php
return [
    'package' => 'core_dictionaries',
    'title' => ['ru' => 'Информационные списки', 'en' => 'Information lists', 'es' => 'Listas de información'],
    'route' => '__#',
    'icon' => 'zmdi zmdi-collection-bookmark',
    'menu_child' => STALKER_CMS\Core\Dictionaries\Http\Controllers\ModuleMenuController::class
];