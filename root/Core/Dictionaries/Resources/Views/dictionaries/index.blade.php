@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_dictionaries::menu.icon') }}"></i> {!! array_translate(config('core_dictionaries::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ $dictionaries_icon }}"></i> {!! $dictionaries_title !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="{{ $dictionaries_icon }}"></i> {!! $dictionaries_title !!}</h2>
    </div>
    @BtnAdd('core.dictionaries.create')
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('core_dictionaries_lang::dictionaries.list')
                </div>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($dictionaries as $dictionary)
                    <div class="js-item-container list-group-item media">
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{!! route('core.dictionaries.edit', $dictionary->id) !!}">
                                            @lang('core_dictionaries_lang::dictionaries.edit')
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{!! route('core.dictionaries.lists_index', $dictionary->id) !!}">
                                            @lang('core_dictionaries_lang::dictionaries.lists')
                                        </a>
                                    </li>
                                    @if(count($dictionary->lists) == 0)
                                        <li class="divider"></li>
                                        <li>
                                            <a class="c-red js-item-remove" href="">
                                                @lang('core_dictionaries_lang::dictionaries.delete.submit')
                                            </a>
                                            {!! Form::open(['route' => ['core.dictionaries.destroy', $dictionary->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                            <button type="submit"
                                                    data-question="@lang('core_dictionaries_lang::dictionaries.delete.question') &laquo;{{ $dictionary->title }}&raquo;?"
                                                    data-confirmbuttontext="@lang('core_dictionaries_lang::dictionaries.delete.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('core_dictionaries_lang::dictionaries.delete.cancelbuttontext')">
                                            </button>
                                            {!! Form::close() !!}
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">{!! $dictionary->title !!}</div>
                            <ul class="lgi-attrs">
                                <li>ID: @numDimensions($dictionary->id)</li>
                                <li>
                                    @lang('core_dictionaries_lang::dictionaries.slug'):
                                    {!! $dictionary->slug !!}
                                </li>
                                <li>
                                    @lang('core_dictionaries_lang::dictionaries.elements'):
                                    {!! count($dictionary->lists) !!}
                                </li>
                            </ul>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('core_dictionaries_lang::dictionaries.empty')</h2>
                @endforelse
            </div>
        </div>
    </div>
@stop