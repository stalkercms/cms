@if(isset($element['name']))
    @if(isset($element['redactor']) && $element['redactor'])
        <div class="form-group">
            <p class="c-gray m-b-20">{{ $element['placeholder'] }}</p>
            {!! Form::textarea('fields['.$element['name'].']', isset($element['value']) ? $element['value'] : NULL, ['class' => 'redactor']) !!}
        </div>
    @else
        <div class="form-group fg-float">
            <div class="fg-line">
                {!! Form::textarea('fields['.$element['name'].']', isset($element['value']) ? $element['value'] : NULL, ['class' => 'form-control auto-size fg-input', 'data-autosize-on' => 'true', 'rows' => 1]) !!}
                <label class="fg-label">{!! $element['placeholder'] !!}</label>
            </div>
        </div>
    @endif
@else
    <div class="m-t-15 clearfix">
        <mark>
            {!! $element['placeholder'] !!}. @lang('core_dictionaries_lang::lists.variable_not_set')
        </mark>
    </div>
@endif