<div class="form-group">
    @if(isset($element['name']))
        <p class="c-gray m-b-5">{!! $element['placeholder'] !!}</p>
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="line-height: 150px;">
                @if(isset($element['value']) && !empty($element['value']))
                    <div class="ci-avatar">
                        <img style="width: 100%" alt="" src="{!! asset('uploads' . $element['value']) !!}">
                    </div>
                @endif
            </div>
            <div>
                <div class="btn btn-info btn-file">
                    <span class="fileinput-new">@lang('core_dictionaries_lang::elements.image.image_select')</span>
                    <span class="fileinput-exists">@lang('core_dictionaries_lang::elements.image.image_change')</span>
                    {!! Form::file($element['name']) !!}
                </div>
                <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">
                    @lang('core_dictionaries_lang::elements.image.image_delete')
                </a>
            </div>
        </div>
    @else
        <div class="m-t-15 clearfix">
            <mark>
                {!! $element['placeholder'] !!}. @lang('core_dictionaries_lang::lists.variable_not_set')
            </mark>
        </div>
    @endif
</div>