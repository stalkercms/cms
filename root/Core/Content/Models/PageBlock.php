<?php
namespace STALKER_CMS\Core\Content\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель Информационные блоки
 * Class PageBlock
 * @package STALKER_CMS\Core\Content\Models
 */
class PageBlock extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'content_pages_blocks';
    protected $fillable = ['slug', 'page_id', 'title', 'content', 'user_id', 'created_at', 'updated_at'];
    protected $hidden = [];
    protected $guarded = [];

    /**
     * @param $request
     * @return $this
     */
    public function insert($request) {

        $this->slug = $request::input('slug');
        $this->page_id = $request::input('page_id');
        $this->title = $request::input('title');
        $this->content = $request::input('content');
        $this->user_id = \Auth::user()->id;
        $this->save();
        return $this;
    }

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->title = $request::input('title');
        $model->content = $request::input('content');
        $model->user_id = \Auth::user()->id;
        $model->save();
        $model->touch();
        return $model;
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /**
     * Шаблон
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function template() {

        return $this->hasOne('\STALKER_CMS\Core\Content\Models\PageTemplate', 'id', 'template_id');
    }

    /**
     * Страница
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function page() {

        return $this->belongsTo('\STALKER_CMS\Core\Content\Models\Page', 'page_id', 'id');
    }

    /**
     * Автор
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['slug' => 'required', 'page_id' => 'required'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return ['page_id' => 'required'];
    }
}