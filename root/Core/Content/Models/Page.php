<?php
namespace STALKER_CMS\Core\Content\Models;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;
use STALKER_CMS\Core\OpenGraph\Traits\OpenGraphTrait;
use STALKER_CMS\Core\Seo\Traits\SeoTrait;

/**
 * Модель страница
 * Class Page
 * @package STALKER_CMS\Core\Content\Models
 */
class Page extends BaseModel implements ModelInterface {

    use ModelTrait, OpenGraphTrait, SeoTrait;
    protected $table = 'content_pages';
    protected $fillable = [
        'slug', 'locale', 'template_id', 'publication', 'start_page', 'title',
        'seo_title', 'seo_description', 'seo_keywords', 'seo_h1', 'seo_url',
        'open_graph', 'published_at', 'user_id', 'created_at', 'updated_at'
    ];
    protected $hidden = [];
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'published_at'];

    /**
     * @param $request
     * @return $this
     */
    public function insert($request) {

        $this->slug = $request::input('slug');
        $this->locale = \App::getLocale();
        $this->template_id = $request::input('template_id');
        $this->publication = $request::has('publication') ? TRUE : FALSE;
        $this->start_page = $request::has('start_page') ? TRUE : FALSE;
        $this->title = $request::input('title');
        $this->seo_title = $request::has('seo_title') && $request::input('seo_title') != '' ? $request::input('seo_title') : NULL;
        $this->seo_keywords = $request::has('seo_keywords') && $request::input('seo_keywords') != '' ? $request::input('seo_keywords') : NULL;
        $this->seo_description = $request::has('seo_description') && $request::input('seo_description') != '' ? $request::input('seo_description') : NULL;
        $this->seo_h1 = $request::has('seo_h1') && $request::input('seo_h1') != '' ? $request::input('seo_h1') : NULL;
        $this->seo_url = $request::has('seo_url') && $request::input('seo_url') != '' ? $request::input('seo_url') : NULL;
        $this->open_graph = $request::has('open_graph') ? $request::input('open_graph') : NULL;
        $this->published_at = $request::has('publication') ? Carbon::now() : NULL;
        $this->user_id = \Auth::user()->id;
        $this->save();
        return $this;
    }

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->template_id = $request::input('template_id');
        $model->start_page = $request::has('start_page') ? TRUE : FALSE;
        $model->title = $request::input('title');
        $model->seo_title = $request::has('seo_title') && $request::input('seo_title') != '' ? $request::input('seo_title') : NULL;
        $model->seo_keywords = $request::has('seo_keywords') && $request::input('seo_keywords') != '' ? $request::input('seo_keywords') : NULL;
        $model->seo_description = $request::has('seo_description') && $request::input('seo_description') != '' ? $request::input('seo_description') : NULL;
        $model->seo_h1 = $request::has('seo_h1') && $request::input('seo_h1') != '' ? $request::input('seo_h1') : NULL;
        $model->seo_url = $request::has('seo_url') && $request::input('seo_url') != '' ? $request::input('seo_url') : NULL;
        $model->open_graph = $request::input('open_graph');
        if($request::has('publication') && !$model->publication):
            $model->published_at = Carbon::now();
        else:
            $model->published_at = NULL;
        endif;
        $model->publication = $request::has('publication') ? TRUE : FALSE;
        $model->user_id = \Auth::id();
        $model->save();
        $model->touch();
        return $model;
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        PageBlock::wherePageId($id)->delete();
        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /**
     * Проверяет доступность URL для страницы
     * @param array $urls
     * @throws \Exception
     */
    public function forbiddenURLs(array $urls) {

        if(!empty($urls)):
            foreach($urls as $url):
                if(in_array($url, config('core_content::config.forbidden_urls'))):
                    throw new \Exception(trans('root_lang::codes.2507'), 2507);
                endif;
            endforeach;
        endif;
    }

    /**
     * Возвращает URL страницы
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getPageUrlAttribute() {

        $prefix = \App::getLocale();
        if(\App::getLocale() == settings(['core_system', 'settings', 'base_locale'])):
            $prefix = '';
        endif;
        if($this->attributes['start_page']):
            return url($prefix);
        elseif(!empty($this->attributes['seo_url'])):
            return url($prefix.'/'.$this->attributes['seo_url']);
        else:
            return url($prefix.'/'.$this->attributes['slug']);
        endif;
    }

    /**
     * Возвращает относительный URL страницы
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getPageRelativeUrlAttribute() {

        $prefix = \App::getLocale();
        if(\App::getLocale() == settings(['core_system', 'settings', 'base_locale'])):
            $prefix = '';
        endif;
        if($this->attributes['start_page']):
            return '/'.$prefix;
        elseif(!empty($this->attributes['seo_url'])):
            return '/'.$prefix.'/'.$this->attributes['seo_url'];
        else:
            return '/'.$prefix.'/'.$this->attributes['slug'];
        endif;
    }

    /**
     * Возвращает символьное имя страницы
     * @return string
     */
    public function getPageSlugAttribute() {

        if($this->attributes['start_page']):
            return 'public.page.index';
        else:
            return 'public.page.'.$this->attributes['slug'];
        endif;
    }

    /**
     * Дата публикации страницы
     * @return string
     */
    public function getPublishedDateAttribute() {

        if($this->attributes['publication']):
            Carbon::setLocale(\App::getLocale());
            return Carbon::parse($this->attributes['published_at'])->diffForHumans();
        else:
            return '<span class="c-red">'.trans('core_content_lang::pages.not_published').'</span >';
        endif;
    }

    /**
     * Цвет страницы
     * @return string
     */
    public function getClassColorAttribute() {

        if($this->attributes['start_page']):
            return 'purple';
        elseif($this->attributes['publication']):
            return 'green';
        else:
            return 'bluegray';
        endif;
    }

    /**
     * Шаблон страницы
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function template() {

        return $this->hasOne('\STALKER_CMS\Core\Content\Models\PageTemplate', 'id', 'template_id');
    }

    /**
     * Информационные блоки страницы
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function blocks() {

        return $this->hasMany('\STALKER_CMS\Core\Content\Models\PageBlock', 'page_id', 'id');
    }

    /**
     * Автор страницы
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['slug' => 'required', 'template_id' => 'required', 'title' => 'required'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return ['template_id' => 'required', 'title' => 'required'];
    }
}