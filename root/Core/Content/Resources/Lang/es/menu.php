<?php
return [
    'list' => 'Enumerar los menús',
    'edit' => 'Editar',
    'embed' => 'Código de inserción',
    'delete' => [
        'question' => 'Eliminar menú',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'items' => 'Elementos de menú',
    'slug' => 'Simbólico código',
    'symbolic_code' => 'Simbólico código',
    'empty' => 'Lista está vacía',
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de menú',
        'form' => [
            'title' => 'Título',
            'template' => 'Plantilla de menú',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. Ejemplo: about',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar de menú',
        'form' => [
            'title' => 'Título',
            'template' => 'Plantilla de menú',
            'submit' => 'Guardar'
        ]
    ]
];