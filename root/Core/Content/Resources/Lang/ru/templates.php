<?php
return [
    'all_types' => 'Все типы',
    'root_directory' => 'Корневой каталог: /home/Resources/Views',
    'edit' => 'Редактировать',
    'delete' => [
        'question' => 'Удалить шаблон',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'empty' => 'Список пустой',
    'insert' => [
        'breadcrumb' => 'Добавить',
        'title' => 'Добавление шаблона',
        'form' => [
            'content' => 'Контент шаблона',
            'template_type' => 'Тип шаблона',
            'title' => 'Название',
            'title_help_description' => 'Например: Главная страница',
            'path' => 'Имя файла шаблона',
            'path_help_description' => 'Расширение .blade.php указывать не нужно!<br>Например: index',
            'submit' => 'Сохранить'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Редактировать',
        'title' => 'Редактирование шаблона',
        'form' => [
            'content' => 'Контент шаблона',
            'title' => 'Название',
            'title_help_description' => 'Например: Главная страница',
            'submit' => 'Сохранить'
        ]
    ]
];