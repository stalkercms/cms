@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! array_translate(config('core_content::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_content::menu.menu_child.pages.icon') }}"></i> {!! array_translate(config('core_content::menu.menu_child.pages.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_content::menu.menu_child.pages.icon') }}"></i>
            {!! array_translate(config('core_content::menu.menu_child.pages.title')) !!}
        </h2>
    </div>
    @if(\PermissionsController::allowPermission('core_content', 'create', FALSE))
        @BtnAdd('core.content.pages.create')
    @endif
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('core_content_lang::pages.list')
                </div>
                @if($pages->count())
                    {!! Form::open(['route' => 'core.content.pages.index', 'method' => 'get']) !!}
                    <div class="ah-search">
                        <input type="text" name="search" placeholder="@lang('core_content_lang::pages.search')" class="ahs-input">
                        <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                    </div>
                    {!! Form::close() !!}
                @endif
                <ul class="actions">
                    @if($pages->count())
                        <li>
                            <a href="" data-ma-action="action-header-open">
                                <i class="zmdi zmdi-search"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-asc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{!! route('core.content.pages.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'asc'])) !!}">
                                        @lang('core_content_lang::pages.sort_title')
                                    </a>
                                </li>
                                <li>
                                    <a href="{!! route('core.content.pages.index', array_merge(Request::all(), ['sort_field' => 'published_at', 'sort_direction' => 'asc'])) !!}">
                                        @lang('core_content_lang::pages.sort_published')
                                    </a>
                                </li>
                                <li>
                                    <a href="{!! route('core.content.pages.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'asc'])) !!}">
                                        @lang('core_content_lang::pages.sort_updated')
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-desc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{!! route('core.content.pages.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'desc'])) !!}">
                                        @lang('core_content_lang::pages.sort_title')
                                    </a>
                                </li>
                                <li>
                                    <a href="{!! route('core.content.pages.index', array_merge(Request::all(), ['sort_field' => 'published_at', 'sort_direction' => 'desc'])) !!}">
                                        @lang('core_content_lang::pages.sort_published')
                                    </a>
                                </li>
                                <li>
                                    <a href="{!! route('core.content.pages.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'desc'])) !!}">
                                        @lang('core_content_lang::pages.sort_updated')
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    <li class="dropdown">
                        <a data-toggle="dropdown" href="" aria-expanded="false">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a class="js-perform-action" href="{!! route('core.content.cache.clear') !!}">
                                    @lang('core_content_lang::pages.cache_clear')
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($pages as $page)
                    <div class="js-item-container list-group-item media">
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    @if(\PermissionsController::allowPermission('core_content', 'edit', FALSE))
                                        <li>
                                            <a href="{!! route('core.content.pages.edit', $page->id) !!}">
                                                @lang('core_content_lang::pages.edit')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{!! route('core.content.pages.blocks_index', $page->id) !!}">
                                                @lang('core_content_lang::pages.blocks')
                                            </a>
                                        </li>
                                    @endif
                                    @if($page->publication)
                                        <li>
                                            <a href="{!! \PublicPage::changeLocale(\App::getLocale(), $page) !!}" target="_blank">
                                                @lang('core_content_lang::pages.blank')
                                            </a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="javascript:void(0);" class="js-copy-link" data-clipboard-text="{!! '@'.'anchor(\'' . $page->slug . '\', \'' . $page->title . '\')' !!}">
                                            @lang('core_content_lang::pages.embed')
                                        </a>
                                    </li>
                                    @if(\PermissionsController::allowPermission('core_content', 'delete', FALSE))
                                        <li class="divider"></li>
                                        <li>
                                            <a class="c-red js-item-remove" href="">
                                                @lang('core_content_lang::pages.delete.submit')
                                            </a>
                                            {!! Form::open(['route' => ['core.content.pages.destroy', $page->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                            <button type="submit"
                                                    data-question="@lang('core_content_lang::pages.delete.question') &laquo;{{ $page->title }}&raquo;?"
                                                    data-confirmbuttontext="@lang('core_content_lang::pages.delete.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('core_content_lang::pages.delete.cancelbuttontext')">
                                            </button>
                                            {!! Form::close() !!}
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">{!! $page->title !!}</div>
                            @if($page->template)
                                <small class="lgi-text">
                                    {!! double_slash('/home/Resources/Views/' .$locale_prefix . '/' . $page->template->path) !!}
                                </small>
                            @endif
                            <ul class="lgi-attrs">
                                <li>ID: @numDimensions($page->id)</li>
                                <li>
                                    @lang('core_content_lang::pages.published'):
                                    {!! $page->PublishedDate !!}
                                </li>
                                <li>
                                    @lang('core_content_lang::pages.update'):
                                    {!! $page->UpdatedDate !!}
                                </li>
                                <li>
                                    @lang('core_content_lang::pages.slug'):
                                    {!! $page->slug !!}
                                </li>
                            </ul>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('core_content_lang::pages.empty')</h2>
                @endforelse
                <div class="lg-pagination p-10">
                    {!! $pages->appends(\Request::only(['search', 'sort_field', 'sort_direction']))->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop