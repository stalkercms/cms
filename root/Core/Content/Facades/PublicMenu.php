<?php
namespace STALKER_CMS\Core\Content\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Фасад контроллера меню
 * Class PublicMenu
 * @package STALKER_CMS\Core\Content\Facades
 */
class PublicMenu extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicMenuController';
    }
}