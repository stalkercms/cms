@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_mailer::menu.icon') }}"></i> {!! array_translate(config('core_mailer::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_mailer::menu.menu_child.mailer.icon') }}"></i> {!! array_translate(config('core_mailer::menu.menu_child.mailer.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_mailer::menu.menu_child.mailer.icon') }}"></i>
            {!! array_translate(config('core_mailer::menu.menu_child.mailer.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('core_mailer_lang::mailer.list')
                </div>
                @if($mails->count())
                    <ul class="actions">
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-asc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{!! route('core.mailer.index', array_merge(Request::all(), ['sort_field' => 'created_at', 'sort_direction' => 'asc'])) !!}">
                                        @lang('core_mailer_lang::mailer.sort_date_created')
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-desc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{!! route('core.mailer.index', array_merge(Request::all(), ['sort_field' => 'created_at', 'sort_direction' => 'desc'])) !!}">
                                        @lang('core_mailer_lang::mailer.sort_date_created')
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                @endif
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($mails as $mail)
                    <div class="js-item-container list-group-item media">
                        <div class="profile-pic small-image pull-left">
                            @ProfileAvatar($mail->name, $mail->avatar)
                        </div>
                        <div class="media-body">
                            <div class="lgi-title">{!! $mail->name !!}</div>
                            <small class="lgi-small">{!! $mail->message !!}</small>
                            @if(!empty($mail->link))
                                <small class="lgi-small">{!! $mail->link !!}</small>
                            @endif
                            <ul class="lgi-attrs">
                                @if(!empty($mail->phone))
                                    <li>{!! $mail->phone !!}</li>
                                @endif
                                @if(!empty($mail->email))
                                    <li>{!! $mail->email !!}</li>
                                @endif
                                @if(!empty($mail->extended_fields))
                                    @foreach($mail->extended_fields as $field)
                                        <li>{!! $field !!}</li>
                                    @endforeach
                                @endif
                                <li>{!! $mail->CreatedDate !!}</li>
                            </ul>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('core_mailer_lang::mailer.empty')</h2>
                @endforelse
                <div class="lg-pagination p-10">
                    {!! $mails->appends(\Request::only(['sort_field', 'sort_direction']))->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop