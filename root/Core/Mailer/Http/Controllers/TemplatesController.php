<?php
namespace STALKER_CMS\Core\Mailer\Http\Controllers;

use STALKER_CMS\Core\Mailer\Models\MailTemplate;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

/**
 * Контроллер Шаблон письма
 * Class TemplatesController
 * @package STALKER_CMS\Core\Mailer\Http\Controllers
 */
class TemplatesController extends ModuleController implements CrudInterface {

    protected $model;
    protected $locale_prefix;
    protected $views_path;

    /**
     * TemplatesController constructor.
     * @param MailTemplate $mailTemplate
     */
    public function __construct(MailTemplate $mailTemplate) {

        parent::__construct();
        $this->model = $mailTemplate;
        $this->views_path = base_path('home/Resources/Mails/'.$this->locale_prefix);
        \PermissionsController::allowPermission('core_mailer', 'templates');
        $this->middleware('auth');
    }

    /**
     * @return View
     */
    public function index() {

        $templates = $this->model->whereLocale(\App::getLocale())->orderBy('updated_at', 'DESC')->get();
        return view('core_mailer_views::templates.index', compact('templates'));
    }

    /**
     * @return View
     */
    public function create() {

        $template_content = '';
        if(view()->exists('core_mailer_views::templates.sketch')):
            $template_content = \File::get(view('core_mailer_views::templates.sketch')->getPath());
        endif;
        return view('core_mailer_views::templates.create', compact('template_content'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function store() {

        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            if(\File::exists($this->views_path) === FALSE):
                \File::makeDirectory($this->views_path, 0754, TRUE);
            endif;
            $view_path = double_slash($this->views_path.'/'.$request::input('path').'.blade.php');
            if(\File::exists($view_path) === FALSE):
                $request::merge(['path' => $request::input('path').'.blade.php', 'required' => FALSE, 'locale' => \App::getLocale()]);
                $this->model->insert($request);
                \File::put($view_path, $request::input('content'));
                return \ResponseController::success(1600)->redirect(route('core.mailer.templates.index'))->json();
            else:
                return \ResponseController::error(2610)->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return View
     */
    public function edit($id) {

        $template_content = '';
        $template = $this->model->findOrFail($id);
        $view_path = realpath(double_slash($this->views_path.'/'.$template->path));
        if(\File::exists($view_path)):
            $template_content = \File::get($view_path);
        endif;
        return view('core_mailer_views::templates.edit', compact('template_content', 'template'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id) {

        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $template = $this->model->findOrFail($id);
            $view_path = realpath(double_slash($this->views_path.'/'.$template->path));
            \File::put($view_path, $request::input('content'));
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('core.mailer.templates.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \RequestController::isAJAX()->init();
        try {
            $template = $this->model->findOrFail($id);
            $view_path = realpath(double_slash($this->views_path.'/'.$template->path));
            if(\File::exists($view_path)):
                \File::delete($view_path);
            endif;
            $this->model->remove($id);
            return \ResponseController::success(1203)->redirect(route('core.mailer.templates.index'))->json();
        } catch(\Exception $e) {
            return \ResponseController::success(2503)->json();
        }
    }
}