<?php
\Route::group(['prefix' => 'admin/mailer', 'middleware' => 'secure'], function() {

    \Route::get('/inbox', ['as' => 'core.mailer.index', 'uses' => 'MailerInboxController@index']);
    \Route::resource('templates', 'TemplatesController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.mailer.templates.index',
                'create' => 'core.mailer.templates.create',
                'store' => 'core.mailer.templates.store',
                'edit' => 'core.mailer.templates.edit',
                'update' => 'core.mailer.templates.update',
                'destroy' => 'core.mailer.templates.destroy'
            ]
        ]
    );
});
\Route::group(['prefix' => 'mail', 'middleware' => 'public'], function() {

    Route::post('feedback', ['as' => 'core.mailer.feedback', 'uses' => 'PublicMailerController@feedback']);
});