@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_uploads::menu.icon') }}"></i> {!! array_translate(config('core_uploads::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_uploads::menu.menu_child.types.icon') }}"></i> {!! array_translate(config('core_uploads::menu.menu_child.types.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_uploads::menu.menu_child.types.icon') }}"></i>
            {!! array_translate(config('core_uploads::menu.menu_child.types.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs"></div>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @foreach($types as $index => $type)
                    <div class="list-group-item media{!! $type->enabled ? '' : ' bgm-bluegray' !!}">
                        <div class="pull-left">
                            <i class="{!! $type->icon !!}"></i>
                        </div>
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        @if($type->enabled)
                                            <a class="c-red js-item-remove" href="">
                                                @lang('core_uploads_lang::types.disabled.submit')
                                            </a>
                                            {!! Form::open(['route' => ['core.uploads.types.destroy', $type->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                            {!! Form::hidden('enabled', FALSE) !!}
                                            <button type="submit"
                                                    data-question="@lang('core_uploads_lang::types.disabled.question') &laquo;{{ $type->title }}&raquo;?"
                                                    data-confirmbuttontext="@lang('core_uploads_lang::types.disabled.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('core_uploads_lang::types.disabled.cancelbuttontext')">
                                            </button>
                                            {!! Form::close() !!}
                                        @else
                                            <a class="c-red js-item-remove" href="">
                                                @lang('core_uploads_lang::types.enabled.submit')
                                            </a>
                                            {!! Form::open(['route' => ['core.uploads.types.update', $type->id], 'method' => 'POST', 'class' => 'hidden']) !!}
                                            {!! Form::hidden('enabled', TRUE) !!}
                                            <button type="submit"
                                                    data-question="@lang('core_uploads_lang::types.enabled.question') &laquo;{{ $type->title }}&raquo;?"
                                                    data-confirmbuttontext="@lang('core_uploads_lang::types.enabled.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('core_uploads_lang::types.enabled.submit')">
                                            </button>
                                            {!! Form::close() !!}
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">{!! $type->title !!}</div>
                            @if(!empty($type->mime_type))
                                <small class="lgi-text{!! $type->enabled ? '' : ' c-white' !!}">
                                    <strong>
                                        @lang('core_uploads_lang::types.mime_type'):
                                    </strong> {!! str_replace('|',', ', $type->mime_type) !!}
                                </small>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop