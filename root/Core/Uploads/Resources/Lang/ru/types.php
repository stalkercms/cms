<?php
return [
    'mime_type' => 'Mime-тип',
    'disabled' => [
        'question' => 'Запретить загружать',
        'confirmbuttontext' => 'Да, запретить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Запретить',
    ],
    'enabled' => [
        'question' => 'Разрешить загружать',
        'confirmbuttontext' => 'Да, разрешить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Разрешить',
    ]
];