<?php
namespace STALKER_CMS\Core\Uploads\Http\Controllers;

use STALKER_CMS\Vendor\Http\Controllers\Controller;
use STALKER_CMS\Core\Uploads\Models\FileTypes;

/**
 * Основной контроллер пакета
 * Class ModuleController
 * @package STALKER_CMS\Core\Uploads\Http\Controllers
 */
abstract class ModuleController extends Controller {

    /**
     * Возвращает список доступных типов файлов для загрузки
     * @return array
     */
    public static function getAllowedFileTypes() {

        $maxFileSize = settings(['core_uploads', 'uploads', 'upload_max_file_size']);
        $file_types = [];
        foreach(FileTypes::whereEnabled(TRUE)->get() as $type):
            if($type->max_size == '' || $type->max_size == 0):
                $type->max_size = $maxFileSize;
            endif;
            $mimes = explode('|', $type->mime_type);
            if(count($mimes)):
                foreach($mimes as $mime):
                    $file_types[$mime] = [
                        'id' => $type->id,
                        'title' => $type->title,
                        'mime' => $mime,
                        'extensions' => $type->extensions,
                        'max_size' => $type->max_size
                    ];
                endforeach;
            endif;
        endforeach;
        sort($file_types);
        return $file_types;
    }
}