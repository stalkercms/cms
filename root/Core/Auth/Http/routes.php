<?php
Route::group(['middleware' => 'secure'], function() {

    $this->get('login', ['as' => 'auth.login.index', 'uses' => 'AuthController@showLoginForm']);
    $this->post('login', ['as' => 'auth.login.authenticate', 'uses' => 'AuthController@login']);
    $this->get('logout', ['as' => 'auth.login.logout', 'uses' => 'AuthController@logout']);
    $this->get('activation', ['as' => 'auth.login.activation', 'uses' => 'AuthController@activation']);
    $this->get('password/reset/{token?}', ['as' => 'auth.password.reset', 'uses' => 'PasswordController@showResetForm']);
    $this->post('password/email', ['as' => 'auth.password.email', 'uses' => 'PasswordController@sendResetLinkEmail']);
    $this->post('password/reset', ['as' => 'auth.password.reset', 'uses' => 'PasswordController@reset']);
});