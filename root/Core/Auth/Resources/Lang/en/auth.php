<?php
return [
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds',
    'welcome' => 'Welcome',
];