<?php
namespace STALKER_CMS\Core\Auth\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

/**
 * Class ModuleServiceProvider
 * @package STALKER_CMS\Core\Auth\Providers
 */
class ModuleServiceProvider extends ServiceProvider {

    protected $policies = ['STALKER_CMS\Core\Auth\Model' => 'STALKER_CMS\Core\Auth\Policies\ModelPolicy'];

    /**
     * Метод загрузки
     * @param GateContract $gate
     */
    public function boot(GateContract $gate) {

        $this->registerPolicies($gate);
        $this->loadViewsFrom(realpath(__DIR__.'/../Resources/Views'), 'core_auth_views');
        $this->loadTranslationsFrom(realpath(__DIR__.'/../Resources/Lang'), 'core_auth_lang');
        $this->registerConfig('core_auth::config', 'Config/auth.php');
        $this->registerSettings('core_auth::settings', 'Config/settings.php');
    }

    /**
     * Метод регистрации
     */
    public function register() {
    }

    /******************************************************************************************************/
    /**
     * @param $namespace
     * @param $config_file
     */
    private function registerConfig($namespace, $config_file) {

        $userConfigFile = app()->configPath().'/auth.php';
        $packageConfigFile = realpath(__DIR__.'/../'.$config_file);
        $config = $this->app['files']->getRequire($packageConfigFile);
        if(file_exists($userConfigFile)):
            $userConfig = $this->app['files']->getRequire($userConfigFile);
            $config = array_replace_recursive($userConfig, $config);
        endif;
        $this->app['config']->set('auth', $config);
        $this->app['config']->set($namespace, $config);
    }

    /**
     * @param $namespace
     * @param $file
     */
    private function registerSettings($namespace, $file) {

        $packageConfigFile = realpath(__DIR__.'/../'.$file);
        $config = $this->app['files']->getRequire($packageConfigFile);
        $this->app['config']->set($namespace, $config);
    }
}
