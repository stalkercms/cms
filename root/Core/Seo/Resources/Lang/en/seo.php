<?php
return [
    'seo' => 'Search Engine Optimization',
    'paste' => 'Fill',
    'seo_title' => 'Title',
    'seo_keywords' => 'Key words',
    'seo_description' => 'Description',
    'seo_h1' => 'Title of the first level',
    'seo_url' => 'Universal Resource Identifier',
    'seo_url_help_description' => 'Page address. Only latin characters, underscores, dashes. Specify without domain name.<br> For example: product-catalog'
];