@extends('root_views::layouts.errors')
@section('title', trans('core_install_lang::break.page_title'))
@section('description', trans('core_install_lang::break.page_description'))
@section('content')
    <div class="four-zero">
        <div class="fz-block">
            <h2>@lang('core_install_lang::break.title')</h2>
            <small class="f-20">@lang('core_install_lang::break.message')</small>
        </div>
    </div>
@stop