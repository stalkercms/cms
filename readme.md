### STALKER CMS RC1.2 на базе PHP Framework Laravel 5.2 (Поддержка прекращена)

##### CMS реализована на базе Laravel PHP Framework версии 5.2 и использует Composer для управления зависимостями. Поэтому прежде чем ставить CMS вы должны установить Composer (https://getcomposer.org/). 

##### Установка Grapheme STALKER CMS 2.0.5.2 RC1.2 при помощи Composer:
`composer --prefer-dist create-project grphm/stalker_rc <ПУТЬ К КАТАЛОГУ ПРОЕКТА> dev-master`

##### Требований к серверу:
* PHP >= 5.5.9
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* Fileinfo PHP Extension
* ImageMagick PHP Extension
* Mcrypt PHP Extension
* Filter PHP Extension
* Session PHP Extension
* Phar PHP Extension
* MySQL 5.5+
* Apache 2.2+ с активным модулем mod_rewrite
* Использование PHP в командной строке

##### Рекомендуется установить и включить следующие разширения PHP:

* cURL PHP Extension
* Iconv PHP Extension
* Json PHP Extension
* SimpleXML PHP Extension
* Zip PHP Extension

Корень сайта должен указывать на каталог public.

Установка STALKER CMS происходит средствами мастера установки. 
Для этого нужно в браузере перейти по ссылке http://mysite.com/install и следовать дальнейшим инструкциям.
База данных автоматически не создается и должна быть создана до начала установки CMS.

##### Если понадобится деинсталировать STALKER CMS воспользуйтесь коммандой artisan: php artisan Uninstall

#### Ссылки для ознакомления:
1. Документация по CMS: https://grapheme2.atlassian.net/wiki/spaces/stalkercms
2. Официальная документация Laravel: https://laravel.com/docs/5.2
3. Документация Laravel API: https://laravel.com/api/5.2/index.html
4. Русская документация Laravel: http://laravel.su/docs/5.0/installation или https://laravel.ru/docs/v5/
5. Laravel Packagist - https://packagist.org/packages/laravel/framework
6. Laravel 5 Fundamentals: https://laracasts.com/series/laravel-5-fundamentals